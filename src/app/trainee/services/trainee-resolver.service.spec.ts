import { TestBed, inject } from '@angular/core/testing';

import { TraineeResolverService } from './trainee-resolver.service';

describe('TraineeResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TraineeResolverService]
    });
  });

  it('should be created', inject([TraineeResolverService], (service: TraineeResolverService) => {
    expect(service).toBeTruthy();
  }));
});
