// angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
// our stuff
import { ErrorService } from '../../core/error.service';
import { iTraineeDetails } from '../interfaces/iTraineeDetails';
import { Itrainee } from '../interfaces/itrainee';
import { of } from 'rxjs/observable/of';
import { Ihealth } from '../interfaces/ihealth';
import { Isession } from '../../program/interfaces/isession';
@Injectable()
export class TraineeService {
  currentTrainee: Itrainee;
  constructor(private _http: HttpClient, private _error: ErrorService) { }
  //////////////////////////////////////////////////////////////////////////////////
  // Get Trainee HTTP request from data base
  //////////////////////////////////////////////////////////////////////////////////
  getTrainee(userName: string): Observable<Itrainee> {
    // if (this.currentTrainee.traineeDetails.userName === '') {
      const obj = JSON.stringify({ 'userName': userName });
      return this._http.post<any>('http://localhost/Refit/php/Core/Trainee/AngularConnection/getTrainee.php',
        obj)
        .map(response => {
          // insert successful if the response
          return response;
        })
        .pipe(
          // http error
          catchError(this._error.handleHttpError('', 'getTrainee', null))
        );
  }
  setTrainee(trainee: iTraineeDetails): Observable<string> {
    const obj = JSON.stringify(trainee);
    if (trainee) {
      return this._http.post<iTraineeDetails>('http://localhost/Refit/php/Core/Trainee/AngularConnection/setTraineeDetails.php',
        obj)
        .map(response => {
          // insert successful if the response
          // php return success if it could set the trainee
          return response;
        })
        .pipe(
          // http error
          catchError(this._error.handleHttpError('', 'setTrainee', null))
        );
    } else {
      // it's happen only if it's error on subscribe
      return of('fail');
    }
  }

  getHealthCondition(userName: string): Observable<Ihealth> {
      const obj = JSON.stringify({ 'userName': userName });
      return this._http.post<string>('http://localhost/Refit/php/Core/Trainee/AngularConnection/getHealth.php',
        obj)
        .map(response => {
          // insert successful if the response
          return response;
        })
        .pipe(
          // http error
          catchError(this._error.handleHttpError('', 'getHealthCondition', null))
        );
  }

  setHealthCondition(health: Ihealth): Observable<string> {

    const obj = JSON.stringify(health);
    return this._http.post<Ihealth>('http://localhost/Refit/php/Core/health_condition/AngularConnection/setHealth.php',
      obj)
      .map(response => {
        // insert successful if the response
        console.log('HTTP response is ' + response);
        return response;
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'setTrainee', null))
      );
  }
  updateHealthCondition(health: Ihealth): Observable<string> {
    const obj = JSON.stringify(health);
    console.log('amin2');
    return this._http.post<Ihealth>('http://localhost/Refit/php/Core/health_condition/AngularConnection/updateHealth.php',
      obj)
      .map(response => {
        // insert successful if the response
        return response;
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'updateTrainee', null))
      );
  }
  setGoal(traineeDetails: iTraineeDetails): Observable<string> {
    console.log(traineeDetails);
    const obj = JSON.stringify(traineeDetails);
    return this._http.post<iTraineeDetails>('http://localhost/Refit/php/Core/Trainee/AngularConnection/setTarget.php',
    obj)
    .map(response => {
      // insert successful if the response
      return response;
    })
    .pipe(
      // http error
      catchError(this._error.handleHttpError('', 'updateTarget', null))
    );
  }
  setProgram(traineeDetails: iTraineeDetails): Observable<string> {
    const obj = JSON.stringify(traineeDetails);
    return this._http.post<iTraineeDetails>('http://localhost/Refit/php/Core/Program/AngularConnection/setProgram.php',
    obj)
    .map(response => {
      // insert successful if the response
      return response;
    })
    .pipe(
      // http error
      catchError(this._error.handleHttpError('', 'updateTarget', null))
    );
  }
  getFirstLastSession(userName: string): Observable<number[]> {
      const obj = JSON.stringify({ 'userName': userName });
      return this._http.post<string>('http://localhost/Refit/php/Core/Session/AngularConnection/firstLastSession.php',
        obj)
        .map(response => {
          // insert successful if the response
          return response;
        })
        .pipe(
          // http error
          catchError(this._error.handleHttpError('', 'getHealthCondition', null))
        );
  }
}