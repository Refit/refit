import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { TraineeService } from './trainee.service';
import { AuthService } from '../../user/services/auth.service';
import { ErrorService } from '../../core/error.service';
import { Itrainee } from '../interfaces/itrainee';
@Injectable()
export class TraineeResolverService implements Resolve<Itrainee> {
  constructor(private _traineeService: TraineeService, private _authService: AuthService,
    private _error: ErrorService, private _router: Router) { }
  //////////////////////////////////////////////////////////////////////////////////
  // make request for trainee from data base.
  // when the response is here, return the trainee and activate the route
  // waiting for trainee.
  // in case of error stay in the home route
  //////////////////////////////////////////////////////////////////////////////////
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Itrainee> {
    return this._traineeService.getTrainee(this._authService.currentUser.userName)
      .map(data => {
        // successful if the response
        if (data) {
          return data; // return trainee
        } else {
          // something wrong with the code
          this._router.navigate(['/home']);
          return null;
        }
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'resolveTrainee', null))
      );
  }
}