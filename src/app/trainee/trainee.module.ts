// angular modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// our modules
import { SharedModule } from '../shared/shared.module';
// libraries
import { MaterialAppModule } from '../ngmaterial.module';
import { PrimengModule } from '../primeng.module';
// our components
import {ScheduleModule} from 'primeng/schedule';
import { WorkdaysComponent } from './components/workdays/workdays.component';
import { ProgressChartComponent } from './components/progress-chart/progress-chart.component';
import { StatusComponent } from './components/status/status.component';
import { ShowDetailsComponent } from './components/show-details/show-details.component';
import { HealthConditionComponent } from './components/health-condition/health-condition.component';
import { GoalSelectComponent } from './components/goal-select/goal-select.component';
import { DoProgramComponent } from './components/do-program/do-program.component';
import { BodyIconsComponent } from './components/body-icons/body-icons.component';
import { BodyPartsComponent } from './components/body-parts/body-parts.component';
import { ProgressMainComponent } from './components/progress-main/progress-main.component';
@NgModule({
  imports: [
    CommonModule,
    MaterialAppModule,
    PrimengModule,
    FormsModule,
    SharedModule,
    ScheduleModule,
  ],
  declarations: [
    WorkdaysComponent,
    ProgressChartComponent,
    StatusComponent,
    ShowDetailsComponent,
    HealthConditionComponent,
    GoalSelectComponent,
    DoProgramComponent,
    ProgressChartComponent,
    BodyIconsComponent,
    BodyPartsComponent,
    ProgressMainComponent,
  ],
  exports: [
    ShowDetailsComponent,
    HealthConditionComponent,
    GoalSelectComponent,
    DoProgramComponent,
    ProgressChartComponent,
    BodyPartsComponent,
  ],
  providers: [
  ]
})
export class TraineeModule { }