import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoProgramComponent } from './do-program.component';

describe('DoProgramComponent', () => {
  let component: DoProgramComponent;
  let fixture: ComponentFixture<DoProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
