import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { TraineeService } from '../../services/trainee.service';
import { iTraineeDetails } from '../../interfaces/iTraineeDetails';
import { messageListTrainee } from '../../message';
import { messageList } from '../../../messages';
import { MenuService } from '../../../me/services/menu.service';
@Component({
  selector: 'do-program',
  templateUrl: './do-program.component.html',
  styleUrls: ['./do-program.component.scss']
})
export class DoProgramComponent implements OnInit {
  activateBuild: boolean;
  activateSession: boolean;
  trainee: iTraineeDetails;
  constructor(private _traineeService: TraineeService, private _messageService: MessageService , private _menuService: MenuService) { }
  ngOnInit() {
    this.trainee = JSON.parse(JSON.stringify(this._traineeService.currentTrainee.traineeDetails));
    this.activateBuild = this._menuService.activateMenu.build;
    // check goal was set (in previous sessions)
    // we are here first time after login.
    // going to next steps and back, will send us to AfterContentInit and not here
  }
  buildProgram() {
    this._messageService.clear();
    this._traineeService.setProgram(this.trainee).subscribe(
      data => {
        // sucessfuly submitted if got here and data is not empty
        if (data && data.length) {
          if (data.toLowerCase() !== 'success') {
            this._messageService.add(messageListTrainee['targetNotSent']);
          } else {
            this._messageService.add(messageListTrainee['targetSent']);
            // update trainee local data
            this._traineeService.currentTrainee.traineeDetails = JSON.parse(JSON.stringify(this.trainee));
          }
        }
      },
      // exceptional termination of the observable sequence
      error => {
        // this message is defined in the app messages.ts (global app message)
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }
}