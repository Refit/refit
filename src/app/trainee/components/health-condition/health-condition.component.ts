import {
  Component,
  OnInit,
  HostListener,
  EventEmitter,
  Output
} from '@angular/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { Ihealth } from '../../interfaces/ihealth';
import { TraineeService } from '../../services/trainee.service';
import { screenSize } from '../../../core/screens';
import { categoryList } from '../../../exercises/interfaces/iexercise';
import { messageListTrainee } from '../../message';
import { messageList } from '../../../messages';
import { MenuService } from '../../../me/services/menu.service';
@Component({
  selector: 'trainee-health-condition',
  templateUrl: './health-condition.component.html',
  styleUrls: ['./health-condition.component.scss']
})
export class HealthConditionComponent implements OnInit {
  healthCondition: Ihealth;
  @Output()
  validTrainee = new EventEmitter();
  showPic: boolean = true;
  innerWidth: number; // width of the screen. used to show svg
  categories: string[] = categoryList; // used by template
  male: boolean = true;
  validForm: boolean;
  colors: string[] = [
    'rgb(165, 165, 165)',
    'rgb(68, 248, 137)',
    'rgb(62, 165, 70)',
    'rgb(39, 112, 72)',
    'rgb(5, 0, 0)'
  ];
  hoverClasses: any[] = [];
  //////////////////////////////////////////////////////////////////////////////////
  // screen resize
  //////////////////////////////////////////////////////////////////////////////////
  // this event happens when screen is resized
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.setShowPic();
  }
  //////////////////////////////////////////////////////////////////////////////////
  // constructor
  //////////////////////////////////////////////////////////////////////////////////
  constructor(
    private _traineeService: TraineeService,
    private _messageService: MessageService,
    private _menuService: MenuService
  ) { }
  //////////////////////////////////////////////////////////////////////////////////
  // get traineee from the service. we can be here after trainee was already
  // extracted from the data base.
  // we are here first time after login.
  // get trainee after resolver, show/hide picture, check all details are valid
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    this.healthCondition = JSON.parse(JSON.stringify(this._traineeService.currentTrainee.traineeDetails.healthCondition));
    // console.log(this._traineeService.currentTrainee.traineeDetails);
    // check goal was set (in previous sessions)
    // we are here first time after login.
    // going to next steps and back, will send us to AfterContentInit and not here
    this.validForm = this.isValid();
    if (this.validForm) {
      // this.validTrainee.emit(); // allow next step processing
      // workaround for check detection bug/feature
      setTimeout(() => {
        this.validTrainee.emit(); // set next step to valid
      }, 100);
    }
    // show figure for wide screen only
    this.innerWidth = window.innerWidth;
    this.setShowPic();
    // initialize classes for hover effect on zones while hovering on body part
    for (let i = 0; i < this.categories.length; i++) {
      const obj = {};
      obj[this.categories[i]] = false;
      this.hoverClasses.push(obj);
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // we are here every time we go back to this view
  //////////////////////////////////////////////////////////////////////////////////
  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterContentInit() {
    // if we go to next step and return here,
    // we need to update the fields again - the fields that were submitted
    // in case user touch some fields go to next step and return,
    // the fields return back to their original submitted state
    // deep copy
    this.healthCondition = JSON.parse(JSON.stringify(this._traineeService.currentTrainee.traineeDetails.healthCondition));
  }
  //////////////////////////////////////////////////////////////////////////////////
  // don't show body on small screens
  //////////////////////////////////////////////////////////////////////////////////
  setShowPic(): void {
    if (this.innerWidth < screenSize['medium-upper-boundary']) {
      this.showPic = false;
    } else {
      this.showPic = true;
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // check all health fields are set to allow next step
  //////////////////////////////////////////////////////////////////////////////////
  isValid(): boolean {
    for (let i = 0; i < this.categories.length; i++) {
      // + at the beginning converts string to number (WE get it as string)
      if (+this.healthCondition[this.categories[i]] === 0) {
        return false;
      }
    }
    return true;
  }
  //////////////////////////////////////////////////////////////////////////////////
  // update health field after user settin
  //////////////////////////////////////////////////////////////////////////////////
  setHealth(zone: string): void {
    this.healthCondition[zone] = (+this.healthCondition[zone] + 1) % 5;
    if (this.healthCondition[zone] === 0) {
      this.healthCondition[zone] = 1;
    }
    this.validForm = this.isValid();
  }
  //////////////////////////////////////////////////////////////////////////////////
  // update health color in template
  //////////////////////////////////////////////////////////////////////////////////
  getHealthColor(zone: string): string {
    return this.colors[this.healthCondition[zone]];
  }
  //////////////////////////////////////////////////////////////////////////////////
  // reset all fields
  //////////////////////////////////////////////////////////////////////////////////
  onReset(): void {
    // learn foreach on array - goes over the entire array (no break is possible)
    this.categories.forEach(cat => {
      this.healthCondition[cat] = 0;
    });
    this.validForm = false;
  }
  //////////////////////////////////////////////////////////////////////////////////
  // submit to data base after all fields are set
  //////////////////////////////////////////////////////////////////////////////////
  onSubmit(): void {
    // clear old messages if they are still active
    this._messageService.clear();
    this._traineeService.updateHealthCondition(this.healthCondition).subscribe(
      data => {
        // sucessfuly submitted if got here and data is not empty
        if (data && data.length) {
          if (data.toLowerCase() !== 'success') {
            this._messageService.add(messageListTrainee['healthConditionNotSent']);
          } else {
            this._messageService.add(messageListTrainee['healthConditionSent']);
            this.validTrainee.emit();
            // update trainee local data
            this._traineeService.currentTrainee.traineeDetails.healthCondition = JSON.parse(JSON.stringify(this.healthCondition));
            this._menuService.setMenu(this._traineeService.currentTrainee.traineeDetails);
          }
        }
      },
      // exceptional termination of the observable sequence
      error => {
        console.log(error);
        // this message is defined in the app messages.ts (global app message)
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }
}