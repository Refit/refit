import { Component, OnInit, EventEmitter, Output, AfterContentInit, HostListener } from '@angular/core';
import { IgoalMood } from '../../interfaces/igoal-mood';
import { TraineeService } from '../../services/trainee.service';
import { iTraineeDetails } from '../../interfaces/iTraineeDetails';
import { MessageService } from 'primeng/components/common/messageservice';
import { messageListTrainee } from '../../message';
import { messageList } from '../../../messages';
import { MenuService } from '../../../me/services/menu.service';
import { screenSize } from '../../../core/screens';
@Component({
  selector: 'trainee-goal-select',
  templateUrl: './goal-select.component.html',
  styleUrls: ['./goal-select.component.scss']
})
export class GoalSelectComponent implements OnInit, AfterContentInit {
  trainee: iTraineeDetails;
  validForm: boolean;
  @Output() validTrainee = new EventEmitter();
  moods: IgoalMood[];
  innerWidth: number; // width of the screen
  phone: boolean;
  //////////////////////////////////////////////////////////////////////////////////
  // screen resize
  //////////////////////////////////////////////////////////////////////////////////
  // this event happens when screen is resized
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.setSubmitButtonLocation();
  }
  constructor(private _traineeService: TraineeService, private _messageService: MessageService,
    private _menuService: MenuService) {
    this.moods = [
      { goal: 'Weight Loss', img: 'weightloss', selected: false, state: 'unset' },
      { goal: 'Body shaping', img: 'bodyshaping', selected: false, state: 'unset' },
      { goal: 'Stay healthy', img: 'health', selected: false, state: 'unset' },
      { goal: 'Formation', img: 'power', selected: false, state: 'unset' },
    ];
  }
  //////////////////////////////////////////////////////////////////////////////////
  // we are here first time after login
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    // change submit button location according to screen size
    this.innerWidth = window.innerWidth;
    this.setSubmitButtonLocation();
    this.trainee = JSON.parse(JSON.stringify(this._traineeService.currentTrainee.traineeDetails));
    // check goal was set (in previous sessions)
    // we are here first time after login.
    // going to next steps and back, will send us to AfterContentInit and not here
    this.validForm = this.isValid();
    if (this.validForm) {
      // workaround for check detection bug/feature
      setTimeout(() => {
        this.validTrainee.emit(); // set next step to valid
      }, 100);
    }
    // show selected target (hightlight in template) from previous session
    for (let i = 0; i < this.moods.length; i++) {
      if (this.moods[i].goal === this.trainee.target) {
        this.moods[i].selected = true;
        this.onChangeSelected(this.moods[i]);
      }
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // we are here every time we go back to this view
  //////////////////////////////////////////////////////////////////////////////////
  ngAfterContentInit() {
    // if we go to next step and return here,
    // we need to update the fields again - the fields that were submitted
    // in case user touch some fields go to next step and return,
    // the fields return back to their original submitted state
    // deep copy
    this.trainee = JSON.parse(JSON.stringify(this._traineeService.currentTrainee.traineeDetails));
  }
  //////////////////////////////////////////////////////////////////////////////////
  // choose submit location
  //////////////////////////////////////////////////////////////////////////////////
  setSubmitButtonLocation(): void {
    if (this.innerWidth < screenSize['medium-upper-boundary']) {
      this.phone = true;
    } else {
      this.phone = false;
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // check selection was done
  //////////////////////////////////////////////////////////////////////////////////
  isValid(): boolean {
    if (this.trainee.target !== null && this.trainee.target !== '') {
      return true;
    }
    return false;
  }
  //////////////////////////////////////////////////////////////////////////////////
  // handle user target selection
  //////////////////////////////////////////////////////////////////////////////////
  onChangeSelected(moodTouched: IgoalMood) {
    if (moodTouched.selected) {
      this.moods.forEach(mood => {
        mood.state = 'normal'; // grayed
        mood.selected = false;
      });
      moodTouched.state = 'highlight';
      moodTouched.selected = true;
      // update the trainee data
      this.trainee.target = moodTouched.goal;
      this.validForm = true;
    } else {
      this.moods.forEach(mood => {
        mood.state = 'unset'; // black
        mood.selected = false;
      });
      this.validForm = false;
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // send data to data base
  //////////////////////////////////////////////////////////////////////////////////
  onSubmit(): void {
    // clear old messages if they are still active
    this._messageService.clear();
    this._traineeService.setGoal(this.trainee).subscribe(
      data => {
        // sucessfuly submitted if got here and data is not empty
        if (data && data.length) {
          if (data.toLowerCase() !== 'success') {
            this._messageService.add(messageListTrainee['targetNotSent']);
          } else {
            this._messageService.add(messageListTrainee['targetSent']);
            this.validTrainee.emit(); // send valid to parent - was already sent onValid function
            // update trainee local data
            this._traineeService.currentTrainee.traineeDetails = JSON.parse(JSON.stringify(this.trainee));
            this._menuService.setMenu(this._traineeService.currentTrainee.traineeDetails);
          }
        }
      },
      // exceptional termination of the observable sequence
      error => {
        // this message is defined in the app messages.ts (global app message)
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }
}