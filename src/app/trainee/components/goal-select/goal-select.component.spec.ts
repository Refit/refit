import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoalSelectComponent } from './goal-select.component';

describe('GoalSelectComponent', () => {
  let component: GoalSelectComponent;
  let fixture: ComponentFixture<GoalSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoalSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoalSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
