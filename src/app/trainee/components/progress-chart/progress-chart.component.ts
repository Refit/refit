import { Component, OnInit, ViewChild } from '@angular/core';
import { TraineeService } from '../../../trainee/services/trainee.service';
import { UIChart } from 'primeng/chart';
import { messageListTrainee } from '../../message';
import { messageList } from '../../../messages';
import { MessageService } from 'primeng/components/common/messageservice';
import { MenuService } from '../../../me/services/menu.service';
@Component({
  selector: 'trainee-chart',
  templateUrl: './progress-chart.component.html',
  styleUrls: ['./progress-chart.component.scss']
})
export class ProgressChartComponent implements OnInit {
  activateProgress: boolean;
  active: boolean[] = [true, false];
  @ViewChild('chart') chart: UIChart;
  seTtype: string;
  data: any;
  options: any;
  traineeGraphStrength: number = 20;
  traineeGraphHealth: number = 0;
  programGraphGrade: number;
  constructor(private _traineeService: TraineeService, private _messageService: MessageService, private _menuService: MenuService) {
    this.calcTraineeNumbers();
    this.seTtype = 'doughnut';
    this.data = {
      labels: ['Strength', 'Trainee Health', 'Program Grade'],
      datasets: [
        {
          data: [this.traineeGraphStrength, this.traineeGraphHealth, this.programGraphGrade],
          backgroundColor: [
            'rgb(39, 112, 72)',
            'rgb(75, 191, 105)',
            'rgb(68, 248, 137)'
          ],
          hoverBackgroundColor: [
            'rgb(39, 112, 72, 0.7)',
            'rgb(75, 191, 105, 0.7)',
            'rgb(68, 248, 137, 0.7)'
          ],
        }
      ]
    };
    this.options = {
      title: {
        display: false,
        text: 'Trainee Details',
        fontSize: 16
      },
      legend: {
        position: 'bottom'
      }
    };
  }
  updateBar(event: Event) {
    this.seTtype = 'bar';
    this.active[1] = true;
    this.active[0] = false;
    this.options.legend.display = false;
    this.delay(500);
  }
  updatePolarArea(event: Event) {
    this.seTtype = 'doughnut';
    this.active[0] = true;
    this.active[1] = false;
    this.options.legend.display = true;
    this.delay(500);
  }
  ngOnInit() {
    this.activateProgress = this._menuService.activateMenu.progress;
  }
  delay(ms: number) {
    setTimeout(() => {
      this.changeType();
    }, 500);
  }
  changeType() {
    this.chart.reinit();
  }
  calcTraineeNumbers() {
    this.traineeGraphStrength += this._traineeService.currentTrainee.traineeDetails.strength * 10;
    this.traineeGraphHealth += +this._traineeService.currentTrainee.traineeDetails.healthCondition.abdominals *10;
    this.traineeGraphHealth += +this._traineeService.currentTrainee.traineeDetails.healthCondition.back *10;
    this.traineeGraphHealth += +this._traineeService.currentTrainee.traineeDetails.healthCondition.biceps *10;
    this.traineeGraphHealth += +this._traineeService.currentTrainee.traineeDetails.healthCondition.chest *10;
    this.traineeGraphHealth += +this._traineeService.currentTrainee.traineeDetails.healthCondition.legs *10;
    this.traineeGraphHealth += +this._traineeService.currentTrainee.traineeDetails.healthCondition.shoulders *10;
    this.traineeGraphHealth += +this._traineeService.currentTrainee.traineeDetails.healthCondition.triceps *10;
    this.programGraphGrade = this._traineeService.currentTrainee.traineeDetails.strength * 12;
  }
  buildProgram() {
    this._messageService.clear();
    this._traineeService.setProgram(this._traineeService.currentTrainee.traineeDetails).subscribe(
      data => {
        // sucessfuly submitted if got here and data is not empty
        if (data && data.length) {
          if (data.toLowerCase() !== 'success') {
            this._messageService.add(messageListTrainee['buildProgNOTfinish']);
          } else {
            this._messageService.add(messageListTrainee['buildProgfinish']);
            // update trainee local data
            this._traineeService.currentTrainee.traineeDetails = JSON.parse(JSON.stringify(this._traineeService.currentTrainee.traineeDetails));
          }
        }
      },
      // exceptional termination of the observable sequence
      error => {
        // this message is defined in the app messages.ts (global app message)
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }
}