import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'trainee-body-icons',
  templateUrl: './body-icons.component.html',
  styleUrls: ['./body-icons.component.scss']
})
export class BodyIconsComponent implements OnInit {
  @Input() target: string;
  @Input() stroke: string;
  @Input() opacity: string;
  constructor() { }
  ngOnInit() {
  }
}