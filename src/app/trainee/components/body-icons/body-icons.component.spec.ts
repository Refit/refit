import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyIconsComponent } from './body-icons.component';

describe('BodyIconsComponent', () => {
  let component: BodyIconsComponent;
  let fixture: ComponentFixture<BodyIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
