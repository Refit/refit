import { Component, OnInit, Output, ViewChild, EventEmitter, AfterContentInit } from '@angular/core';
import { iTraineeDetails } from '../../interfaces/iTraineeDetails';
import { TraineeService } from '../../services/trainee.service';
import { NgForm } from '@angular/forms';
import { MessageService } from 'primeng/components/common/messageservice';
import { messageListTrainee } from '../../message';
import { messageList } from '../../../messages';
import { MenuService } from '../../../me/services/menu.service';
@Component({
  selector: 'trainee-show-details',
  templateUrl: './show-details.component.html',
  styleUrls: ['./show-details.component.scss']
})
export class ShowDetailsComponent implements OnInit, AfterContentInit {
  @Output() validTrainee = new EventEmitter();
  @ViewChild('updateTrainee') updateTrainee: NgForm;
  trainee: iTraineeDetails;
  genderType = ['Male', 'Female'];
  statusType = [['Very Active', 4], ['Active', 3], ['Little Active', 2], ['Not Active', 1]];
  workDays = [false, false, false, false, false, false, false];
  startDate: Date = new Date(1990, 0, 1);
  min: number = 2.3;
  wlimits: number[] = [40, 140];
  hlimits: number[] = [1, 2.3];
  hlimitsStr: string = `range [${this.hlimits[0]},${this.hlimits[1]}] meters`;
  wlimitsStr: string = `range [${this.wlimits[0]},${this.wlimits[1]}] kg`;
  traineeDaysEX: string = 'S,,T,,T,,';
  traineeDays: string[];
  fields2Check: string[] = [
    'fname', 'lname', 'age', 'gender', 'weight', 'height', 'status', 'workDays'
  ];
  validForm: boolean = false;
  formError: string = null;
  constructor(private _traineeService: TraineeService, private _messageService: MessageService,
    private _menuService: MenuService) { }
  //////////////////////////////////////////////////////////////////////////////////
  // get traineee from the service. we can be here after trainee was already
  // extracted from the data base.
  // we are here first time after login.
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    this.trainee = JSON.parse(JSON.stringify(this._traineeService.currentTrainee.traineeDetails));
    this.workDays = this.trainee.workDays;
    // check all fields are set (in previous sessions)
    // we are here first time after login.
    // going to next steps and back, will send us to AfterContentInit and not here
    this.validForm = this.isValid();
    if (this.validForm) {
      // workaround for check detection bug/feature
      setTimeout(() => {
        this.validTrainee.emit(); // set next step to valid
      }, 100);
    }
    // follow form changes - on any change call onValid
    // this.updateTrainee.statusChanges.subscribe(val => this.onValid());
  }
  //////////////////////////////////////////////////////////////////////////////////
  // we are here every time we go back to this view
  //////////////////////////////////////////////////////////////////////////////////
  ngAfterContentInit() {
    // if we go to next step and return here,
    // we need to update the fields again - the fields that were submitted
    // in case user touch some fields go to next step and return,
    // the fields return back to their original submitted state
    // deep copy
    this.trainee = JSON.parse(JSON.stringify(this._traineeService.currentTrainee.traineeDetails));
  }
  //////////////////////////////////////////////////////////////////////////////////
  // check form is valid - all fields are completed to allow moving to the next stage
  //////////////////////////////////////////////////////////////////////////////////
  isValid(): boolean {
    let flag: boolean = true;
    Object.keys(this.trainee).forEach(key => {
      if (this.fields2Check.indexOf(key) !== -1) {
        const val = this.trainee[key];
        if ((val === null || val === '') ||
          (key === 'height' && (val < this.hlimits[0] || val > this.hlimits[1])) ||
          (key === 'width' && (val < this.wlimits[0] || val > this.wlimits[1])) ||
          (key === 'workDays' && this.workDays.every(el => el === false))) {
          flag = false;
        }
      }
    });
    return flag;
  }
  //////////////////////////////////////////////////////////////////////////////////
  // update height/width from template
  //////////////////////////////////////////////////////////////////////////////////
  changeHeight(h: number) {
    this.trainee.height = h;
    if (h < this.hlimits[0] || h > this.hlimits[1]) {
      this.validForm = false;
    } else {
      // check form is valid
      this.validForm = this.isValid();
    }
  }
  changeWeight(w: number) {
    this.trainee.weight = w;
    if (w < this.wlimits[0] || w > this.wlimits[1]) {
      this.validForm = false;
    } else {
      // check form is valid
      this.validForm = this.isValid();
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // send the form to the data base
  //////////////////////////////////////////////////////////////////////////////////
  onSubmit(traineeForm: NgForm): void {
    // clear old messages if they are still active
    this._messageService.clear();
    this.formError = null;
    this._traineeService.setTrainee(this.trainee).subscribe(
      data => {
        // sucessfuly submitted if got here and data is not empty
        if (data && data.length) {
          if (data.toLowerCase() !== 'success') {
            this._messageService.add(messageListTrainee['traineeNotSent']);
            this.formError = messageListTrainee['traineeNotSent']['detail'];
          } else {
            this._messageService.add(messageListTrainee['traineeSent']);
            this.formError = messageListTrainee['traineeSent']['detail'];
            this.validTrainee.emit();
            // update trainee local data
            this._traineeService.currentTrainee.traineeDetails = JSON.parse(JSON.stringify(this.trainee));
            this._menuService.setMenu(this._traineeService.currentTrainee.traineeDetails);
          }
        }
      },
      // exceptional termination of the observable sequence
      error => {
        // this message is defined in the app messages.ts (global app message)
        this._messageService.add(messageList['invalidTermination']);
        this.formError = messageList['invalidTermination']['detail'];
      }
    );
  }
  changeStatus(s: string): void {
    this.trainee.status = this.statusType[s];
    // check form is valid
    this.validForm = this.isValid();
  }
  changeDays(days: boolean[]): void {
    // ADD setting
    this.workDays = days;
    this.trainee.workDays = days;
    // check form is valid
    this.validForm = this.isValid();
  }
}