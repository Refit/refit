import { Component, OnInit, ViewChild } from '@angular/core';
import { UIChart } from 'primeng/chart';
import { TraineeService } from '../../services/trainee.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { MenuService } from '../../../me/services/menu.service';
import { Isession } from '../../../program/interfaces/isession';
@Component({
  selector: 'body-parts',
  templateUrl: './body-parts.component.html',
  styleUrls: ['./body-parts.component.scss']
})
export class BodyPartsComponent implements OnInit {
  activateProgress: boolean;
  active: boolean[] = [true, false];
  @ViewChild('chart') chart: UIChart;
  seTtype: string;
  flsessions : number[];
  data: any;
  options: any;
  traineeGraphStrength: number = 20;
  traineeGraphHealth: number = 30;
  programGraphGrade: number = 50;
  constructor(private _traineeService: TraineeService,
     private _messageService: MessageService, private _menuService: MenuService) {
    this.seTtype = 'doughnut';
    this.delay(500);
  }
  ngOnInit() {
 this._traineeService.getFirstLastSession(this._traineeService.currentTrainee.traineeDetails.userName).subscribe(
      data => {
          this.flsessions = data;
          console.log(this.flsessions);
      },
      // exceptional termination of the observable sequence
      error => {
      }
    );
    this.activateProgress = this._menuService.activateMenu.progress;
  }
  delay(ms: number) {
    setTimeout(() => {
        this.data = {
            labels: ['Legs', 'Chest', 'Back', 'Biceps', 'Triceps', 'Shoulders'],
            datasets: [
                {
                    label: 'My first session',
                    backgroundColor: '#42A5F5',
                    borderColor: '#1E88E5',
                    data: [this.flsessions[0],this.flsessions[2],this.flsessions[4],this.flsessions[6],this.flsessions[8],this.flsessions[10]]
                },
                {
                    label: 'My last session',
                    backgroundColor: '#9CCC65',
                    borderColor: '#7CB342',
                    data: [this.flsessions[1],this.flsessions[3],this.flsessions[5],this.flsessions[7],this.flsessions[9],this.flsessions[11]]       
                }
            ]
        }
    }, 500);
  }
}