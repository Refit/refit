import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressMainComponent } from './progress-main.component';

describe('ProgressMainComponent', () => {
  let component: ProgressMainComponent;
  let fixture: ComponentFixture<ProgressMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
