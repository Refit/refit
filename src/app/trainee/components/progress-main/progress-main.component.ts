import { Component, OnInit } from '@angular/core';
import { TraineeService } from '../../services/trainee.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { MenuService } from '../../../me/services/menu.service';
import { iTraineeDetails } from '../../interfaces/iTraineeDetails';
@Component({
  selector: 'progress-main',
  templateUrl: './progress-main.component.html',
  styleUrls: ['./progress-main.component.scss']
})
export class ProgressMainComponent implements OnInit {
  activateProgress: boolean;
  trainee: iTraineeDetails;
  constructor(private _traineeService: TraineeService, private _messageService: MessageService , private _menuService: MenuService) { }
  ngOnInit() {
    this.trainee = JSON.parse(JSON.stringify(this._traineeService.currentTrainee.traineeDetails));
    this.activateProgress = this._menuService.activateMenu.build;
  }
}