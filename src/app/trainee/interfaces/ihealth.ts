export interface Ihealth {
    id?: string;
    userName: string;
    legs: number;
    chest: number;
    shoulders: number;
    back: number;
    biceps: number;
    triceps: number;
    abdominals: number;
}
export const emptyHealth: Ihealth = {
    id: '',
    userName: '',
    legs: 0,
    chest: 0,
    shoulders: 0,
    back: 0,
    biceps: 0,
    triceps: 0,
    abdominals: 0,
};