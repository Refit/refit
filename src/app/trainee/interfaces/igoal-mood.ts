export interface IgoalMood {
    goal: string;
    img: string;
    selected: boolean,
    state: string
}