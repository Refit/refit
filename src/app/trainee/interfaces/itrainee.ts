import { iTraineeDetails, emptyTrainee } from './iTraineeDetails';
export interface Itrainee {
    traineeDetails: iTraineeDetails;
}
export const emptyBaseTrainee: Itrainee = {
    traineeDetails: emptyTrainee,
};