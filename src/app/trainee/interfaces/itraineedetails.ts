import { Ihealth, emptyHealth } from './ihealth';
export interface iTraineeDetails {
    userName: string;
    email: string;
    status: number;
    fname: string;
    lname: string;
    target: string;
    age: number;
    gender: string;
    weight: number;
    height: number;
    bodyFat: number;
    healthCondition: Ihealth;
    intensity: number;
    strength: number;
    workDays: boolean[];
}
export const emptyTrainee: iTraineeDetails = {
    userName: '',
    email: '',
    status: 0,
    fname: '',
    lname: '',
    target: '',
    age: 0,
    gender: 'Male',
    weight: 0,
    height: 0,
    bodyFat: 0,
    healthCondition: emptyHealth,
    intensity: 0,
    strength: 0,
    workDays: [false, false, false, false, false, false, false]
};