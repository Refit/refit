import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PrimengModule } from '../primeng.module';
import { MaterialAppModule } from '../ngmaterial.module';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { TermsComponent } from './components/terms/terms.component';
import { SharedModule } from '../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    PrimengModule,
    MaterialAppModule,

    SharedModule,
  ],
  declarations: [
    AboutUsComponent,
    ContactUsComponent,
    TermsComponent
  ],
  exports: [
    AboutUsComponent,
    ContactUsComponent,
    TermsComponent
  ]
})
export class GlobalModule { }