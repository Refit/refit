import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { slideInAnimation } from '../../../shared/animation/slide-in';
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss'],
  animations: [slideInAnimation],
  // attach the fade in animation to the host (root) element of this component
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[@slideInAnimation]': '{value: ": enter", params: { opacity: 0.9 }}'
  }
})
export class AboutUsComponent implements OnInit {
  marginTop: string;
  constructor(private _location: Location, private _router: Router) { }
  ngOnInit() {
    const href = this._router.url.split('/')[1];
    if (href !== 'welcome') {
      this.marginTop = '0';
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // go to  previous page page on pressing x icon
  //////////////////////////////////////////////////////////////////////////////////
  goBack() {
    this._router.navigate(['/home']);
    // this._location.back();
  }
}