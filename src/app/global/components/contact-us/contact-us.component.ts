import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MessageService } from 'primeng/components/common/messageservice';
import { messageList } from '../../../messages';
import { slideInAnimation } from '../../../shared/animation/slide-in';
import { MailService } from '../../../core/mail.service';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
  animations: [slideInAnimation],
  // attach the fade in animation to the host (root) element of this component
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[@slideInAnimation]': '{value: ": enter", params: { opacity: 0.9 }}'
  }
})
export class ContactUsComponent implements OnInit {
  formError: string = null;
  submitting = false;
  email: string;
  name: string;
  message: string;
  marginTop: string;
  constructor(private _mailService: MailService, private _messageService: MessageService,
        private _router: Router, private _location: Location) { }
  ngOnInit() {
    const href = this._router.url.split('/')[1];
    if (href !== 'welcome') {
      this.marginTop = '0';
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // go to  previous page page on pressing x icon
  //////////////////////////////////////////////////////////////////////////////////
  goBack() {
    this._router.navigate(['/home']);
    // this._location.back();
  }
  //////////////////////////////////////////////////////////////////////////////////
  //
  //////////////////////////////////////////////////////////////////////////////////
  onSubmit(contactUsForm: NgForm) {
    // clear old messages if they are still active
    this._messageService.clear();
    console.log('submitting...', contactUsForm);
    this.formError = null;
    this.submitting = true;
    this._mailService.sendEmail(this.name, this.email, this.message)
      .subscribe(
        data => {
          // console.log(data);
          if (data !== 'success') {
            this._messageService.add(messageList['mailSendError']);
            this.formError = messageList['mailSendError']['detail'];
          } else {
            this.formError = null;
            this._messageService.add(messageList['mailSend']);
          }
          this.submitting = false;
        },
        // exceptional termination of the observable sequence
        error => {
          this._messageService.add(messageList['invalidTermination']);
          this.formError = messageList['invalidTermination']['detail'];
          this.submitting = false;
        });
  }
}
