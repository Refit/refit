import { INavLinks } from '../shared/interfaces/inav-links';

export const navLinksList: INavLinks[] = [
    {
        label: 'Home',
        route: ['home'],
        icon: '',
    },
    {
        label: 'Exercises',
        route: ['exercises'],
        icon: '',
    },
    {
        label: 'Users',
        route: ['users'],
        icon: '',
    },
    {
        label: 'Bugs',
        route: ['debug'],
        icon: '',
    }
];


