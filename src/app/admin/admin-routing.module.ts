import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';
import { AuthGuardService } from '../user/services/auth-guard.service';
import { AddExComponent } from './components/add-ex/add-ex.component';
import { UpdateExComponent } from './components/update-ex/update-ex.component';
import { AdminExComponent } from './components/admin-ex/admin-ex.component';
import { AdminUsersComponent } from './components/admin-users/admin-users.component';
import { DebugComponent } from './components/debug/debug.component';
import { UpdateBugComponent } from './components/update-bug/update-bug.component';
import { AdminCoverComponent } from './components/admin-cover/admin-cover.component';
import { TermsComponent } from '../global/components/terms/terms.component';
import { ContactUsComponent } from '../global/components/contact-us/contact-us.component';
import { AboutUsComponent } from '../global/components/about-us/about-us.component';

// our components

export const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '', redirectTo: 'home', pathMatch: 'full'
      },
      {
        path: 'home',
        component: AdminCoverComponent,
      },
      {
        path: 'terms',
        component: TermsComponent,
      },
      {
        path: 'contact-us',
        component: ContactUsComponent,
      },
      {
        path: 'about',
        component: AboutUsComponent,
      },
      {
        // grouping routing (under dummy router-outlet)
        path: 'debug', // component-less route (without router outlet)
        children: [
          {
            path: '',
            component: DebugComponent,
          },
          {
            path: 'update-bug',
            component: UpdateBugComponent,
          },
          {
            path: 'add-bug',
            component: UpdateBugComponent,
          },
        ]
      },
      {
        // grouping routing (under dummy router-outlet)
        path: 'exercises', // component-less route (without router outlet)
        children: [
          {
            path: '',
            component: AdminExComponent,
          },
          {
            path: 'update-ex',
            component: UpdateExComponent,
          },
          {
            path: 'add-ex',
            component: AddExComponent,
          },
        ]
      },

      {
        path: 'users',
        component: AdminUsersComponent
      },

    ]
  },


];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdminRoutingModule { }



export class UserRoutingModule { }

export const routableComponents = [
  AdminCoverComponent,
  AdminComponent,
  DebugComponent,
  UpdateBugComponent,
  UpdateExComponent,
  AdminExComponent,
  AddExComponent,
  AdminUsersComponent,
];

