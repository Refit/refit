// bug structure
export interface IDebug {
    id?: number;
    userName: string;
    date: string;
    info: string;
    status: string;
    owner: string;
    component: string;
    moduleName: string;
    bugName: string;
    priority: string;
}

// bugs priority structure key-value (priority-bugs array)
export interface BugsByPriority {
    [priority: string]: IDebug[];
}

// empty structure for initializatins
export const emptyDebug: IDebug = {
    userName: '',
    date: '',
    info: '',
    status: 'pending',
    owner: '',
    component: '',
    moduleName: '',
    bugName: '',
    priority: 'low'
};

// all existing priorities
export const priorityList: string[] = [
    'severe', 'high', 'middle', 'low', 'nice to have', 'leave it'
];
