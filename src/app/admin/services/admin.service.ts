// angular
import { Injectable} from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, retry, tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { of } from 'rxjs/observable/of';

// our stuff
import { ErrorService } from '../../core/error.service';
import { IDebug } from '../interfaces/idebug';

@Injectable()
export class AdminService {
    allDebug: IDebug[]; // bugs local storage to save transfer time
    currentBug: IDebug; // current bug to be handled

    // saved info while going out from the bug page,
    // will be retored when we come back to it
    expandedPanelBugs: string = ''; // priority panel to be expanded on page refresh
    addUpdate: string = 'Add'; // identifies if the bug has to be added or just updated
    bugsFilterBy: string = ''; // name of bug owner to be shown on bugs page on page refresh


    constructor(private _http: HttpClient, private _error: ErrorService) { }

    //////////////////////////////////////////////////////////////////////////////////
    // insert bug HTTP request to data base and into the local data base
    //////////////////////////////////////////////////////////////////////////////////
    insertDebug(debug: IDebug): Observable<any> {
        const obj = JSON.stringify(debug);
        // update the local נועד data base
        if (this.allDebug) {
            // if not already exist, add it
            if (this.allDebug.find(bug => bug.bugName === debug.bugName)) {
                return (of('exists'));
            }
            // update local storage
            this.allDebug.push(debug);
        }
        return this._http.post<any>('http://localhost/Refit/php/Core/Debug/AngularConnection/insertDebug.php',
            obj)
            .map(response => {
                // insert successful if the response
                debug.id = response;
                console.log('HTTP response is ' + response);
                return response;
            })
            .pipe(
                // http error
                catchError(this._error.handleHttpError('', 'insertDebug', []))
            );
    }

    //////////////////////////////////////////////////////////////////////////////////
    // HTTP request to update existing bug in the server data base and in the local
    //////////////////////////////////////////////////////////////////////////////////
    updateDebug(debug: IDebug): Observable<any> {
        const obj = JSON.stringify(debug);
        // console.log(obj);
        return this._http.post<any>('http://localhost/Refit/php/Core/Debug/AngularConnection/updateDebug.php',
            obj)
            .map(response => {
                // sugnup successful if the response
                // console.log('HTTP response is ' + response);

                // update local storage
                // lear (array splice) - how to change element in the array
                // array.splice(index, howMany, [element1][, ..., elementN]);
                // this.allDebug.splice(this.allDebug.findIndex(bug => bug.bugName === debug.bugName), 1, debug);
                // actually no need to update since the bug is in the array by referece,
                // so any change will be already there
                return response;
            })
            .pipe(
                // http error
                catchError(this._error.handleHttpError('', 'updateDebug', []))
            );
    }
    /////////////////////////////////////////////////////////////////////////////////
    // delete bug from server data base and also from the local data base
    ////////////////////////////////////////////////////////////////////////////////
    deleteDebug(bug: IDebug): Observable<any> {
        const obj = JSON.stringify(bug);
        // console.log(obj);
        return this._http.post<any>('http://localhost/Refit/php/Core/Debug/AngularConnection/deleteDebug.php',
            obj)
            .map(response => {
                // delete from local data base
                this.allDebug = this.allDebug.filter(debug => debug !== bug);
                // console.log('HTTP response is ' + response);
                return response;
            })
            .pipe(
                // http error
                catchError(this._error.handleHttpError('', 'deleteDebug', []))
            );
    }

    //////////////////////////////////////////////////////////////////////////////////
    // HTTP request get request to extract all bugs from the data base
    //////////////////////////////////////////////////////////////////////////////////
    getDebugTable(): Observable<IDebug[]> {
        // check if already were extracted to save the transfer time
        if (this.allDebug) {
            return of(this.allDebug);
        }
        return this._http.get<IDebug[]>(
            'http://localhost/Refit/php/Core/Debug/AngularConnection/getDebugTable.php')
            .pipe(
                retry(3), // retry a failed request up to 3 times
                tap(data => this.allDebug = data),
                catchError(this._error.handleHttpError('', 'getDebugTable', []))
            );
    }

}
