// angular
import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

// our stuff
import { IDebug } from '../../interfaces/idebug';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-bug',
  templateUrl: './bug.component.html',
  styleUrls: ['./bug.component.scss']
})
export class BugComponent implements OnInit {
  @Input() bug: IDebug; // get bug to handle from parent component
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  //////////////////////////////////////////////////////////////////////////////////
  // material dialog is opened on pressing bug name
  //////////////////////////////////////////////////////////////////////////////////
  openDialog(): void {
    // console.log('amin');
    // console.log(JSON.stringify(this.bug));
    const dialogRef = this.dialog.open(BugDialog, {
      width: '400px',
      data: this.bug,
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

//////////////////////////////////////////////////////////////////////////////////
// dialog component
//////////////////////////////////////////////////////////////////////////////////
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-bug-dialog',
  templateUrl: './bug-dialog.html',
  styleUrls: ['./bug.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class BugDialog {

  constructor(
    public dialogRef: MatDialogRef<BugDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
