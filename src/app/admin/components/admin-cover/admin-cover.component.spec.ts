import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCoverComponent } from './admin-cover.component';

describe('AdminCoverComponent', () => {
  let component: AdminCoverComponent;
  let fixture: ComponentFixture<AdminCoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminCoverComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
