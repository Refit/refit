import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../user/services/users.service';
import { AdminService } from '../../services/admin.service';
import { ExercisesService } from '../../../exercises/services/exercises.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-cover',
  templateUrl: './admin-cover.component.html',
  styleUrls: ['./admin-cover.component.scss']
})
export class AdminCoverComponent implements OnInit {

  constructor(private _userService: UsersService,
    private _adminService: AdminService,
    private _exerciseService: ExercisesService) { }

  ngOnInit() {
    // clear all the saved data if we are here
    this._userService.expandedPanelUserList = '';
    this._adminService.expandedPanelBugs = '';
    this._exerciseService.expandedPanelExList = '';
  }

}
