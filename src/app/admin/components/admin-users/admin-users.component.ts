// angular
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

// libraries
import { MatDialog, MatDialogRef } from '@angular/material';

// our stuff
import { MessageService } from 'primeng/components/common/messageservice';
import { IUser } from '../../../user/interfaces/iuser';
import { UsersService } from '../../../user/services/users.service';
import { messageList } from '../../../messages';
import { ConfirmationDialog } from '../../../shared/components/confirmation-dialog/confirmation-dialog.component';




@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss']
})
export class AdminUsersComponent implements OnInit {
  users: IUser[] = [];
  dialogRef: MatDialogRef<ConfirmationDialog>;

  constructor(private _userService: UsersService, private _router: Router,
    private _messageService: MessageService, public dialog: MatDialog) { }

  //////////////////////////////////////////////////////////////////////////////////
  // get users from the data base and sort them alphanumerically
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this._userService.getUsers()
      .subscribe((users: IUser[]) => {
        this.users = users;
        this.sortUsers();
        // console.log(users);
      },
        // exceptional termination of the observable sequence
        error => {
          // global message defined in app messages.ts
          this._messageService.add(messageList['invalidTermination']);
        }
      );
  }

  //////////////////////////////////////////////////////////////////////////////////
  // sort in ascending alphabetical order
  //////////////////////////////////////////////////////////////////////////////////
  sortUsers(): void {
    // learn (array sort)
    // sort array by exercise name
    this.users.sort((a, b) => {
      if (a.userName > b.userName) {
        return 1;
      }
      if (a.userName < b.userName) {
        return -1;
      }
      return 0;
    });
  }
  //////////////////////////////////////////////////////////////////////////////////
  // edit existing user
  //////////////////////////////////////////////////////////////////////////////////
  public editUser(user: IUser): void {
    this._userService.currentUser = user;
    this._userService.editUser(user)
      .subscribe(
        data => {
          // console.log(data);
          this._messageService.add(messageList['wasSent']);
        },
        // exceptional termination of the observable sequence
        error => {
          this._messageService.add(messageList['invalidTermination']);
        }
      );
  }


  //////////////////////////////////////////////////////////////////////////////////
  // remove user from the data base and locally for display
  //////////////////////////////////////////////////////////////////////////////////
  public removeUser(name: string): void {
    this._userService.removeUser(name).subscribe((isRemoved: number) => {
      if (isRemoved === 1) {
        // learn (Filter, Array) - 3 ways to remove/filter an object by property from an array

        // 1:
        // let i = 0;
        // remove the user from the display
        // this.users.forEach(element => {
        //   if (element.userName === name) {
        //     i = this.users.indexOf(element);
        //     this.users.splice(i, 1);
        //   }
        // });

        // 2:
        // this.users = this.users.filter(obj =>
        //   obj.userName !== name);

        // 3:
        this.users.splice(this.users.findIndex(obj => obj.userName === name), 1);
        // learn (parent->child input) passing non elementary input to chil like array.
        // array change is nnot detected in child component if the reference is the same.
        // the following will cause array reference change and thus, change
        // will be detected in the child component.
        // slice returns the selected elements in an array, as a new array object
        this.users = this.users.slice(0);
      }
    },
      // exceptional termination of the observable sequence
      error => {
        // global message defined in app messages.ts
        this._messageService.add(messageList['invalidTermination']);
      });
  }

  //////////////////////////////////////////////////////////////////////////////////
  // confirmation dialog on delete
  //////////////////////////////////////////////////////////////////////////////////
  openConfirmationDialog(name: string): void {
    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });
    this.dialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // do confirmation actions
        this.removeUser(name);
      }
      this.dialogRef = null;
    });
  }


}


