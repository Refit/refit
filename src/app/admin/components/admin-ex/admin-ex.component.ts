
// angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// libraries
import { MessageService } from 'primeng/components/common/messageservice';
import { MatDialog, MatDialogRef } from '@angular/material';

// our interfaces/services/components
import { ExercisesService } from '../../../exercises/services/exercises.service';
import { IExercise, categoryList } from '../../../exercises/interfaces/iexercise';
import { ICat } from '../../../exercises/interfaces/icat-exercises';
import { messageList } from '../../../messages';
import { ConfirmationDialog } from '../../../shared/components/confirmation-dialog/confirmation-dialog.component';



@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-ex',
  templateUrl: './admin-ex.component.html',
  styleUrls: ['./admin-ex.component.scss']
})
export class AdminExComponent implements OnInit {
  exercises: ICat[];

  // information to be displayed when pressing info icon
  info: string = `
    <p>Exercises are organized by category (legs, chest, ...).</p>
    <p>Press <b>Add Eercise</b> button to add a new exercise.</p>
    <p>To see existing exercises, first press <b>category name</b>, then press:</p>
    <ul><li><i>exercise name</i> - to see exercise details</li>
    <li><i>edit</i> - to update the exercise</li>
    <li><i>delete</i> - to remove the exercise</li></ul>
    `;
  dialogRef: MatDialogRef<ConfirmationDialog>;

  constructor(private _exerciseService: ExercisesService,
    private route: ActivatedRoute, private _router: Router, private _messageService: MessageService,
    public dialog: MatDialog) {
  }

  //////////////////////////////////////////////////////////////////////////////////
  // ngOnInit - get exercises from the data base and sort them alphanumerically
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    // pay attention that after add/update we get here with the new list because
    // of getExercises. thus, there is no need to update display in add/update components
    this.getExercises();
  }

  getExercises(): void {
    // clear old messages if they are still active
    this._messageService.clear();

    this._exerciseService.getExercises()
      .subscribe(
        (exercises: ICat[]) => {
          if (exercises && exercises.length) {
            this.exercises = exercises;
            // console.log(exercises);
            this.sortExercises();
          }
        },
        // exceptional termination of the observable sequence
        error => {
          // global message defined in app messages.ts
          this._messageService.add(messageList['invalidTermination']);
        });
  }

  //////////////////////////////////////////////////////////////////////////////////
  // sort exercises alphanumerically
  //////////////////////////////////////////////////////////////////////////////////
  sortExercises(): void {
    // sort exercise array
    for (let i = 0; i < this.exercises.length; i++) {
      this.exercises[i].exercises.sort((a, b) => {
        if (a.exerciseName > b.exerciseName) {
          return 1;
        }
        if (a.exerciseName < b.exerciseName) {
          return -1;
        }
        return 0;
      });
    }
  }

  //////////////////////////////////////////////////////////////////////////////////
  // edit existing exercise through the dedicated component
  //////////////////////////////////////////////////////////////////////////////////
  public editEx(ex: IExercise): void {
    this._exerciseService.currentEx = ex;
    localStorage.setItem('currentEx', JSON.stringify(this._exerciseService.currentEx));
    this._router.navigate(['update-ex'], { relativeTo: this.route });
  }

  //////////////////////////////////////////////////////////////////////////////////
  // remove exercise from the data base and from the local data structure
  //////////////////////////////////////////////////////////////////////////////////
  public removeEx(ex: IExercise): void {
    const name: string = ex.exerciseName;
    this._exerciseService.removeEx(ex).subscribe(
      (isRemoved: number) => {
        if (isRemoved === 1) {
          // remove fronm the displayed list
          this.exercises.forEach(element => {
            element.exercises = element.exercises.filter(obj =>
              obj.exerciseName !== name);
          });
        }
      },
      // exceptional termination of the observable sequence
      error => {
        // global message defined in app messages.ts
        this._messageService.add(messageList['invalidTermination']);
      });
  }

  //////////////////////////////////////////////////////////////////////////////////
  // confirmation dialog on delete
  //////////////////////////////////////////////////////////////////////////////////
  openConfirmationDialog(ex: IExercise): void {
    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });
    this.dialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // do confirmation actions
        this.removeEx(ex);
      }
      this.dialogRef = null;
    });
  }

}

