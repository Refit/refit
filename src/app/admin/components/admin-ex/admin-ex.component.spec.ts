import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminExComponent } from './admin-ex.component';

describe('AdminExComponent', () => {
  let component: AdminExComponent;
  let fixture: ComponentFixture<AdminExComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminExComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminExComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
