
// angular
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

// libraries
import { MessageService } from 'primeng/components/common/messageservice';

// our stuff
import { ExercisesService } from '../../../exercises/services/exercises.service';
import { IExercise, emptyExercise } from '../../../exercises/interfaces/iexercise';
import { messageList } from '../../../messages';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-add-ex',
  templateUrl: './add-ex.component.html',
  styleUrls: ['./add-ex.component.scss']
})
export class AddExComponent implements OnInit {
  ex: IExercise = Object.assign({}, emptyExercise);


  constructor(private _exercisesService: ExercisesService, private _location: Location,
    private _router: Router, private _messageService: MessageService) { }

  //////////////////////////////////////////////////////////////////////////////////
  // get exercise to edit bythrough  the service
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    this._exercisesService.currentEx = this.ex;
  }
  //////////////////////////////////////////////////////////////////////////////////
  // add new exercise
  //////////////////////////////////////////////////////////////////////////////////
  public addEx(ex: IExercise): void {
    // clear old messages if they are still active
    this._messageService.clear();

    this._exercisesService.addEx(ex).subscribe(
      data => {
        // sucessfuly submitted if got here and data is not empty
        // console.log(data);
        if (data && data.length) {
          this._messageService.add(messageList['wasSent']);
           
        }
      },
      // exceptional termination of the observable sequence
      error => {
        this._messageService.add(messageList['invalidTermination']);
      }
    );
      
    
  }

  //////////////////////////////////////////////////////////////////////////////////
  // return to previous page
  //////////////////////////////////////////////////////////////////////////////////
  goBack(): void {
    this._location.back();
  }

}

