import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateExComponent } from './update-ex.component';

describe('UpdateExComponent', () => {
  let component: UpdateExComponent;
  let fixture: ComponentFixture<UpdateExComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateExComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateExComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
