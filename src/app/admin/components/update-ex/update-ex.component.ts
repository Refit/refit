// angular
import { Component, OnInit} from '@angular/core';
import { Location } from '@angular/common';

// libraries
import { MessageService } from 'primeng/components/common/messageservice';

// our stuff
import { IExercise, emptyExercise } from '../../../exercises/interfaces/iexercise';
import { ExercisesService } from '../../../exercises/services/exercises.service';
import { messageList } from '../../../messages';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-update-ex',
  templateUrl: './update-ex.component.html',
  styleUrls: ['./update-ex.component.scss']
})
export class UpdateExComponent implements OnInit {
  constructor(private _exercisesService: ExercisesService,
    private _messageService: MessageService, private _location: Location) { }
  ex: IExercise;
  //////////////////////////////////////////////////////////////////////////////////
  // restore last exercise on page refresh
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    if (this._exercisesService.currentEx) {
      this.ex = this._exercisesService.currentEx;
    } else {
      // on page refresh restore the last exercise that was edited (before editing)
      this.ex = Object.assign({}, emptyExercise);
      const currentEx = localStorage.getItem('currentEx');
      if (currentEx !== null && currentEx !== '') {
        this.ex = JSON.parse(currentEx);
      }
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // edit existing exercise
  //////////////////////////////////////////////////////////////////////////////////
  editEx(ex: IExercise): void {
    // clear old messages if they are still active
    this._messageService.clear();
    // console.log(ex);
    this._exercisesService.editEx(ex).subscribe(
      data => {
        // sucessfuly submitted if got here and data is not empty
        // console.log('Send bug response ' + data);
        if (data && data.length) {
          this._messageService.add(messageList['wasSent']);
          // setTimeout(() => { this.goBack(); }, 1500);
        }
      },
      // exceptional termination of the observable sequence
      error => {
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }

  //////////////////////////////////////////////////////////////////////////////////
  // return to previous page
  //////////////////////////////////////////////////////////////////////////////////
  goBack(): void {
    this._location.back();
  }
}

