import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BugChangeFormComponent } from './bug-change-form.component';

describe('BugChangeFormComponent', () => {
  let component: BugChangeFormComponent;
  let fixture: ComponentFixture<BugChangeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BugChangeFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BugChangeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
