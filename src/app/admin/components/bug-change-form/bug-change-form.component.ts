// angular
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Location } from '@angular/common';

// our stuff
import { IDebug, emptyDebug, priorityList } from '../../interfaces/idebug';
import { AuthService } from '../../../user/services/auth.service';
import { AdminService } from '../../services/admin.service';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-bug-change-form',
  templateUrl: './bug-change-form.component.html',
  styleUrls: ['./bug-change-form.component.scss']
})
export class BugChangeFormComponent implements OnInit {
  @Output() submitBug = new EventEmitter<IDebug>(); // event to parent component
  @Input() addUpdate: string = 'Add/Update Bug';
  bug: IDebug;  // current bug to handle
  priorityList: string[] = priorityList;  // defined in interfaces
  rtl: string = 'ltr';  // right to left typing direction in description textarea
  bugNameReadOnly: boolean = false;

  constructor(private _authService: AuthService, private _adminService: AdminService,
    private _location: Location) { }

  //////////////////////////////////////////////////////////////////////////////////
  // on component init get the bug to be displayed from the service or get it
  // from the local storage in case of page refresh
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    if (this._adminService.currentBug) {
      this.bug = this._adminService.currentBug;
    } else {
      // on page refresh restore the last bug that was edited (before editing)
      // learn (object copy) - deep object copy
      // use this.bug = Object.assign({}, emptyDebug) for shallow copy
      this.bug = JSON.parse(JSON.stringify(emptyDebug));

      // look is bug was previously stored in the local storage
      // if yes, use it
      this.addUpdate = localStorage.getItem('addUpdate');
      const currentBug = localStorage.getItem('currentBug');
      if (currentBug !== null && currentBug !== '') {
        this.bug = JSON.parse(currentBug);
        this.addUpdate = localStorage.getItem('addUpdate');
      }
    }
    // bug publisher is the current user
    this.bug.userName = this._authService.currentUser.userName;
    if (this.addUpdate === 'Add') {
      this.clearForm();
      this.bugNameReadOnly = false;
    } else {
      this.bugNameReadOnly = true;
    }
  }

  //////////////////////////////////////////////////////////////////////////////////
  // on form submit raise event to parent component
  // on clear - clear form fields. take care of user name and bug name in case
  // of bug update
  //////////////////////////////////////////////////////////////////////////////////
  onSubmit(): void {
    this.submitBug.emit(this.bug);
  }

  clearForm(): void {
    const bugName = this.bug.bugName;
    this.bug = JSON.parse(JSON.stringify(emptyDebug));
    // set bug publisher, which is the current user
    this.bug.userName = this._authService.currentUser.userName;
    if (this.addUpdate === 'Update') {
      // in case we update the existing bug, restore its name after clearing
      this.bug.bugName = bugName;
    }
  }

  //////////////////////////////////////////////////////////////////////////////////
  // go to previous page when x was clicked
  //////////////////////////////////////////////////////////////////////////////////
  goBack(): void {
    this._location.back();
  }

  //////////////////////////////////////////////////////////////////////////////////
  // change left-right text direction for description textarea
  //////////////////////////////////////////////////////////////////////////////////
  changeRtl(): void {
    this.rtl = this.rtl === 'ltr' ? 'rtl' : 'ltr';
  }

}
