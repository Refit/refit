
// angular
import { Component, OnInit, Input, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

// libraries
import { MessageService } from 'primeng/components/common/messageservice';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';  // for csv download
import { MatDialog, MatDialogRef } from '@angular/material';

// our stuff
import { IDebug, emptyDebug, priorityList, BugsByPriority } from '../../interfaces/idebug';
import { AdminService } from '../../services/admin.service';
import { UsersService } from '../../../user/services/users.service';
import { AuthService } from '../../../user/services/auth.service';
import { messageList } from '../../../messages';
import { ConfirmationDialog } from '../../../shared/components/confirmation-dialog/confirmation-dialog.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-debug',
  templateUrl: './debug.component.html',
  styleUrls: ['./debug.component.scss']
})
export class DebugComponent implements OnInit {
  // information to be displayed when pressing the info icon
  info: string = `
    <p>Press <b>Add Bug</b> button to add a new bug.</p>
    <p>Press <b>Download</b> button to download all existing bugs in csv format.</p>
    <p>To see existing bugs, first press <b>severity level</b> (severe, high ... on the left).
    Then press:</p>
    <ul><li><i>bug name</i> - to see the details of the bug</li>
    <li><i>edit</i> - to update the bug</li>
    <li><i>delete</i> - to remove the bug from the list</li></ul>
    <p>You can write an <b>owner name</b> (case insensitive) in the input box
    to get all <i>non completed bugs</i> by this owner in priority order.
    (from highest to lowest).
    To <b>clear</b> the owner list, just empty the owner input box.</p>
    `;

  rows: IDebug[] = null; // bugs list from the data base
  filtered: BugsByPriority = null; // bugs list from the data base
  bugsByPriority: BugsByPriority = {}; // bugs to be displayed sorted by priorities
  priorities: string[] = priorityList; // list of priorities
  expanded: { [p: string]: boolean } = {};
  private _filterBy: string = '';
  filteredCount: number = 0;
  totalBugs: number = 0;

  get filterBy(): string {
    return this._filterBy;
  }

  set filterBy(val: string) {
    this._filterBy = val;
    if (this._filterBy === '') {
      this.filtered = null;   // clear display
    } else if (this._filterBy === this._adminService.bugsFilterBy) {
      // change event is not trigerred after the box is cleared - do it manually
      this.updateFilter();
    }
  }

  // file for bugs output in csv format
  file: Object;
  fileOptions = {
    // fieldSeparator: '"' ,
    // quoteStrings: '.',
    // decimalseparator: true ,
    showLabels: true,
    showTitle: true,
    title: 'Debug',
    // useBom: true ,
    headers: ['id', 'Type', 'Publisher', 'Date', 'Info', 'Status', 'Owner', 'Component', 'Module'],
  };

  dialogRef: MatDialogRef<ConfirmationDialog>;

  // tslint:disable-next-line:max-line-length
  constructor(private _authService: AuthService, private _adminService: AdminService,
    private _messageService: MessageService, private route: ActivatedRoute,
    private _router: Router, public dialog: MatDialog) { }


  //////////////////////////////////////////////////////////////////////////////////
  // get bugs from the data base and sort them by prioritoes and then
  // in alphabetical order in each priority
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    // restore last filter
    // actual restore is after all bugs are in below
    this._filterBy = this._adminService.bugsFilterBy;
    // learn (GET subscribe) - how to subscribe to get from server request
    this.getBugs();
  }

  getBugs(): void {
    // clear old messages if they are still active
    this._messageService.clear();

    // get bug table from the data base
    this.totalBugs = 0;
    this.sortBugsByPriority(new Array()); // generate empty list for display
    this._adminService.getDebugTable().subscribe(
      (data: IDebug[]) => {
        // do stuff with our data here.
        // ....
        // asign data to our class property in the end
        // so it will be available to our template
        if (data && data.length) {
          this.rows = data; // to be displayed
          this.totalBugs = data.length;

          this.sortBugsByPriority(data);
          this.updateFilter();
        }
      },
      // exceptional termination of the observable sequence
      error => {
        // global message defined in app messages.ts
        this._messageService.add(messageList['invalidTermination']);
      }
    );

  }

  //////////////////////////////////////////////////////////////////////////////////
  // sort bugs by priority and in each priority in alphabetical order
  //////////////////////////////////////////////////////////////////////////////////
  sortBugsByPriority(bugs: IDebug[]): void {
    // set which panel was expanded last time we were on this page
    const expandedPriority = this._adminService.expandedPanelBugs;
    this._filterBy = this._adminService.bugsFilterBy;

    // define prioroty keys
    for (let i = 0; i < this.priorities.length; i++) {
      this.bugsByPriority[this.priorities[i]] = new Array();

      // define if panel has to be expanded
      this.expanded[this.priorities[i]] = false;
      if (this.priorities[i] === expandedPriority && this.filterBy === '') {
        this.expanded[this.priorities[i]] = true;
      }

    }

    // fill bugs array as values on key-value pairs
    for (let i = 0; i < bugs.length; i++) {
      this.bugsByPriority[bugs[i].priority].push(bugs[i]);
    }

    // learn (Sort, Array) - how to sort array of objects
    // sort bugs array in alphabetical order
    for (let i = 0; i < this.priorities.length; i++) {
      this.bugsByPriority[this.priorities[i]].sort((a, b) => {
        if (a.bugName > b.bugName) {
          return 1;
        }
        if (a.bugName < b.bugName) {
          return -1;
        }
        return 0;
      });
    }
  }

  //////////////////////////////////////////////////////////////////////////////////
  // set bugs by filterBy owner name. the filtered bugs will be shown
  // under the owner name
  //////////////////////////////////////////////////////////////////////////////////
  updateFilter(): void {
    this._adminService.bugsFilterBy = this._filterBy;
    const filterBy: string = this._filterBy.toLowerCase();

    if (filterBy === '') {
      this.filtered = null;
    } else {
      this.collapseExpanded(); // only filtered panel will be expanded

      this.filtered = {}; // currently empty object

      this.filteredCount = 0;
      for (let i = 0; i < this.priorities.length; i++) {
        // only non completed bugs will be shown
        this.filtered[this.priorities[i]] =
          this.bugsByPriority[this.priorities[i]].filter(obj =>
            obj.owner === filterBy && obj.status !== 'completed');
        this.filteredCount += this.filtered[this.priorities[i]].length;
      }
    }
  }

  //////////////////////////////////////////////////////////////////////////////////
  // call edit/add bug form to update the current bug.
  // pass the bug through admin service to update component
  //////////////////////////////////////////////////////////////////////////////////
  public editBug(operation: string, bug: IDebug): void {
    // set parameters passed through the service
    this._adminService.currentBug = emptyDebug; // default settings on add bug

    if (operation === 'Update') {
      this._adminService.currentBug = bug;  // update existing bug
    } else {
      this._adminService.currentBug = emptyDebug;
    }
    this._adminService.addUpdate = operation; // add/update operation flag

    // learn and remeber: save strings in local storage
    // save in local storage to support page refresh while updating the bug
    localStorage.setItem('currentBug', JSON.stringify(this._adminService.currentBug));
    localStorage.setItem('addUpdate', operation);

    // learn (Routing, relative) - how to navigate from ts file by relative path
    // navigate to the update bug page. it will subscribe to add/update on the server
    this._router.navigate(['update-bug'], { relativeTo: this.route });
  }

  //////////////////////////////////////////////////////////////////////////////////
  // remove bug from the data base
  //////////////////////////////////////////////////////////////////////////////////
  public deleteBug(bug: IDebug): void {
    // learn (Post, Subscribe) - how to subscribe to post request

    // clear old messages if they are still active
    this._messageService.clear();

    // subscribe to post to server.
    // it has to respons with data array
    this._adminService.deleteDebug(bug).subscribe(
      data => {
        // console.log('Debug submit response ' + data);
        // console.log(data);
        if (data && data.length) {
          this.totalBugs = 0;
          // delete also in the local data base for display
          for (let i = 0; i < this.priorities.length; i++) {
            this.bugsByPriority[this.priorities[i]] = this.bugsByPriority[this.priorities[i]]
              .filter(obj => obj.bugName !== bug.bugName);
            this.totalBugs += this.bugsByPriority[this.priorities[i]].length;
          }
          this.updateFilter();
        }
      },
      // exceptional termination of the observable sequence
      error => {
        // this message is defined in the app messages.ts (global app message)
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }

  //////////////////////////////////////////////////////////////////////////////////
  // confirmation dialog on delete
  //////////////////////////////////////////////////////////////////////////////////
  openConfirmationDialog(bug: IDebug): void {
    this.dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });
    this.dialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // do confirmation actions
        this.deleteBug(bug);
      }
      this.dialogRef = null;
    });
  }

  //////////////////////////////////////////////////////////////////////////////////
  // define that panel is expanded/collapsed, thus on go back to this page,
  // it will stay in the same state
  //////////////////////////////////////////////////////////////////////////////////
  setExpanded(p: string): void {
    // name of priority, which panel is expanded in display
    this._adminService.expandedPanelBugs = p;
  }
  collapseExpanded(): void {
    // all panels are collapsed - currently not used
    this._adminService.expandedPanelBugs = '';
  }

  //////////////////////////////////////////////////////////////////////////////////
  // download csv file containing current bugs
  //////////////////////////////////////////////////////////////////////////////////
  download(): void {
    let bugs = [];
    Object.entries(this.bugsByPriority).forEach(([key, value]) => {
      bugs = bugs.concat(value);
    });
    this.file = new Angular5Csv(JSON.stringify(bugs), 'DebugTable', this.fileOptions);
  }

}
