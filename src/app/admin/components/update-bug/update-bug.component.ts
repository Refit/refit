
// angular
import { Component, OnInit} from '@angular/core';
import { Location } from '@angular/common';

// libraries
import { MessageService } from 'primeng/components/common/messageservice';

// our stuff
import { IDebug } from '../../interfaces/idebug';
import { AdminService } from '../../services/admin.service';
import { messageList } from '../../../messages';
import { messageBugList } from '../../messages';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-update-bug',
  templateUrl: './update-bug.component.html',
  styleUrls: ['./update-bug.component.scss']
})
export class UpdateBugComponent implements OnInit {

  // we got here from debug component by navigate.
  // thus, the relevant data below is passed through the admin service
  bug: IDebug = this._adminService.currentBug;
  addUpdate: string;

  constructor(private _adminService: AdminService,
    private _messageService: MessageService, private _location: Location) { }

  ngOnInit() {
    this.addUpdate = this._adminService.addUpdate;

    // get all bugs in case this page was refreshed
    // clear old messages if they are still active
    this._messageService.clear();

    // get bug table from the data base
    this._adminService.getDebugTable().subscribe(
      (data: IDebug[]) => {
        // do stuff with our data here.
        // ....
        // asign data to our class property in the end
        // so it will be available to our template
        if (data && data.length) {

        }
      },
      // exceptional termination of the observable sequence
      error => {
        // global message defined in app messages.ts
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }

  //////////////////////////////////////////////////////////////////////////////////
  // submit a new or edited bug to the data base
  // we got here from bug-change-form emit event.
  // the appropriate event (add or update) is served
  //////////////////////////////////////////////////////////////////////////////////
  onSubmit(bug: IDebug): void {
    // clear old messages if they are still active
    this._messageService.clear();
    if (this.addUpdate === 'Add') {
      this.sendBug(bug);
    } else {
      this.updateBug(bug);
    }
  }

  //////////////////////////////////////////////////////////////////////////////////
  // submit a new bug to the data base
  //////////////////////////////////////////////////////////////////////////////////
  public sendBug(bug: IDebug): void {

    this._adminService.insertDebug(bug).subscribe(
      data => {
        // sucessfuly submitted if got here and data is not empty
        // console.log('Send bug response ' + data);
        if (data && data.length) {
          if (data === 'exists') {
            this._messageService.add(messageBugList['bugExists']);
          } else {
            this._messageService.add(messageList['wasSent']);
            // setTimeout(() => { this.goBack(); }, 1500);
          }
        }
      },
      // exceptional termination of the observable sequence
      error => {
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }

  //////////////////////////////////////////////////////////////////////////////////
  // submit an existing edited bug to the data base
  //////////////////////////////////////////////////////////////////////////////////
  public updateBug(bug: IDebug): void {
    this._adminService.updateDebug(bug).subscribe(
      data => {
        // sucessfuly submitted if got here and data is not empty
        // console.log(data);
        if (data && data.length) {
          this._messageService.add(messageList['wasSent']);
          // in case we want to go to previoud page on submit
          // setTimeout(() => { this.goBack(); }, 1500);
        }
      },
      // exceptional termination of the observable sequence
      error => {
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }

  //////////////////////////////////////////////////////////////////////////////////
  // return to previous page
  //////////////////////////////////////////////////////////////////////////////////
  goBack(): void {
    this._location.back();
  }

}
