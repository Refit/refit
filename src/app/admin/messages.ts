// messages displayed during login and signup processes
export const messageBugList = {
    bugExists: {
        // severity: sucess, info, error, warn
        // summary - message title
        severity: 'error',
        summary: '',
        detail: 'this bug already exists'
    },
    // add more messages here if required
};
