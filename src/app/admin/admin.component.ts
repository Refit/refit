// angular
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

// our stuff
import { ICat } from '../exercises/interfaces/icat-exercises';
import { ExercisesService } from '../exercises/services/exercises.service';
import { SharedService } from '../shared/services/shared.service';
import { AdminService } from './services/admin.service';
import { UsersService } from '../user/services/users.service';

import { IUser } from '../user/interfaces/iuser';
import { navLinksList } from './navigation-links';
import { INavLinks } from '../shared/interfaces/inav-links';
import { slideInAnimation } from '../shared/animation/slide-in';



@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  animations: [slideInAnimation],
})
export class AdminComponent implements OnInit {
  navLinks: INavLinks[] = navLinksList;
  linkColor: string = '#fff';
  exercises: ICat[];
  filteredExercises: ICat[];
  users: IUser[];
  padding: string;
  maxWidth: string;

  animationFromLocation = {
    value: 'test', params: { perX: '100', perY: '0' }
  };

  constructor(private _exerciseService: ExercisesService, private _sharedService: SharedService,
    private _userService: UsersService, private _Router: Router,
    private _adminService: AdminService) {
      const href = this._Router.url.split('/').pop();

      if (href === 'contact-us' || href === 'about' || href === 'terms') {
        this.padding = '0';
      }
      if (href === 'contact-us' ) {
        this.maxWidth = '600px';
      }
     }

  ngOnInit() {
    // allow Route animations for navigating to the same route, but different parameters
    // By using this code the page is going to refresh if you clicked
    // on the same route no matter what is the parameter you added to the route.
    // thus, animation is working on each page transition!
    this._Router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this._Router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        this._Router.navigated = false;
        window.scrollTo(0, 0);
      }
    });

    // prepare exercises and user data to be showed in admin
    this.getExercises();
    this.getUsers();

    // send color to subscribers via observable subject
    // this._sharedService.clearMessage();
    this._sharedService.sendMessage('#fff', 'footerColor');

    // clear service storage variables - currently will not be used
    this._adminService.expandedPanelBugs = '';
    this._adminService.bugsFilterBy = '';
    this._userService.expandedPanelUserList = '';
    this._userService.filterBy = '';

  }

  // learn (Filter) - filter example - currently not used
  performFilter(filterBy?: string): void {
    if (filterBy) {
      this.filteredExercises = this.exercises.filter((cat: ICat) =>
        cat.category.indexOf(filterBy) !== -1);
    } else {
      this.filteredExercises = this.exercises;
    }
  }


  // subscribe to get exercises from http. the actual get is done in exercise service
  getExercises() {
    this._exerciseService.getExercises()
      .subscribe((exercises: ICat[]) => {
        this.exercises = exercises;
        this.filteredExercises = exercises;
        // console.log(exercises);
      });
  }

  // subscribe to get users from http. the actual get is done in user service
  getUsers() {
    this._userService.getUsers()
      .subscribe((users: IUser[]) => {
        this.users = users;
        // console.log(users);
      });
  }

  // get x and y location in percentages from where navigation starts
  // slide in animation
  getAnimationDirection(): any {
    return this.animationFromLocation;
  }

}




