// angular modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


// libraries
import { MaterialAppModule } from '../ngmaterial.module';
import { PrimengModule } from '../primeng.module';

// modules
import { AdminRoutingModule, routableComponents } from './admin-routing.module';
import { ExercisesModule } from '../exercises/exercises.module';
import { SharedModule } from '../shared/shared.module';
import { UserModule } from '../user/user.module';

// components
import { AdminComponent } from './admin.component';
import { BugChangeFormComponent } from './components/bug-change-form/bug-change-form.component';
import { BugComponent, BugDialog } from './components/bug/bug.component';
import { DebugComponent } from './components/debug/debug.component';

// services
import { AdminService } from './services/admin.service';
import { GlobalModule } from '../global/global.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,

    PrimengModule,
    MaterialAppModule,

    UserModule,
    SharedModule,
    ExercisesModule,
    GlobalModule,

    AdminRoutingModule,
  ],
  declarations: [
    routableComponents,
    BugComponent,
    BugDialog,
    BugChangeFormComponent,
  ],
  providers: [
    AdminService,
  ],
  exports: [
    AdminComponent,
    DebugComponent,
    BugComponent,
  ],
  entryComponents: [
    // An entry component is any component that Angular loads imperatively,
    // (which means you’re not referencing it in the template), by type
    // BugComponent,
    BugDialog,
  ],
})
export class AdminModule { }
