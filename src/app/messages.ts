// messages displayed during login and signup processes
export const messageList = {
    mailSendError: {
        // severity: sucess, info, error, warn
        // summary - message title
        severity: 'error',
        summary: '',
        detail: 'invalid email'
    },
    mailSend:
    {
        severity: 'info',
        summary: '',
        detail: 'mail was sent'
    },
    wasSent:
    {
        severity: 'info',
        summary: '',
        detail: 'was sent successfully'
    },
    invalidTermination:
    {
        severity: 'error',
        summary: '',
        detail: 'operation failed'
    }
    // add more messages here if required
};