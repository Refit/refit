// angular modules
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
// libraries
import { PrimengModule } from './primeng.module';
import { MessageService } from 'primeng/components/common/messageservice';
import { MaterialAppModule } from './ngmaterial.module';
// our modules
import { AppRoutingModule, routedComponents } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { HomeModule } from './home/home.module';
import { UserModule } from './user/user.module';
import { AdminModule } from './admin/admin.module';
import { SharedModule } from './shared/shared.module';
// services
import { AuthService } from './user/services/auth.service';
import { AuthGuardService } from './user/services/auth-guard.service';
// components
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { MailService } from './core/mail.service';
import { SharedService } from './shared/services/shared.service';
import { GlobalModule } from './global/global.module';
@NgModule({
  imports: [
    // --all modules that i used in this module--
    // angular modules
    BrowserAnimationsModule, // included by SharedModule (BrowserAnimationsModule includes BrowserModule)
    CoreModule,
    PrimengModule,
    MaterialAppModule,
    FormsModule,
    // our modules
    HomeModule,
    UserModule,
    AdminModule,
    GlobalModule,
    SharedModule,
    // put after all modules with routhing because of routing to ** in this one
    AppRoutingModule
  ],
  declarations: [
    // components , directives , pipes
    AppComponent,
    FooterComponent,
    routedComponents,
  ],
  providers: [
    AuthService,
    AuthGuardService,
    MessageService,
    MailService,
    SharedService
  ],
  // first running  component
  bootstrap: [AppComponent]
})
export class AppModule { }