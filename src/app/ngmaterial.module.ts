import { NgModule } from '@angular/core';
import { CdkTableModule } from '@angular/cdk/table';
import { MatButtonModule, MatMenuModule, MatIconModule, MatDialogModule } from '@angular/material';
import { MatFormFieldModule, MatNativeDateModule , MatInputModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSortModule } from '@angular/material/sort';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { MatChipsModule } from '@angular/material/chips';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSliderModule } from '@angular/material/slider';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSidenavModule } from '@angular/material/sidenav';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
@NgModule({
  imports: [
    MatButtonModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatMenuModule,
    MatIconModule ,
    MatTabsModule ,
    MatSortModule ,
    MatExpansionModule ,
    MatCardModule ,
    MatFormFieldModule ,
    MatNativeDateModule,
    ReactiveFormsModule,
    CdkTableModule ,
    MatInputModule ,
    MatChipsModule ,
    MatRadioModule ,
    MatSelectModule,
    MatTooltipModule,
    MatDialogModule,
    MatSliderModule,
    MatDatepickerModule,
    MatSidenavModule,
    OrganizationChartModule,
    MatProgressSpinnerModule,
  ],
  exports: [
    MatButtonModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatMenuModule,
    MatIconModule ,
    MatTabsModule ,
    MatSortModule ,
    MatExpansionModule ,
    MatCardModule ,
    MatFormFieldModule ,
    MatNativeDateModule,
    ReactiveFormsModule,
    CdkTableModule ,
    MatInputModule ,
    MatChipsModule ,
    MatRadioModule ,
    MatSelectModule,
    MatTooltipModule,
    MatDialogModule,
    MatSliderModule,
    MatDatepickerModule,
    MatSidenavModule,
    OrganizationChartModule,
    MatProgressSpinnerModule,
  ]
})
export class MaterialAppModule { }