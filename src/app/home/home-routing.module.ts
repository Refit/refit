import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { AuthGuardService } from '../user/services/auth-guard.service';
import { TermsComponent } from '../global/components/terms/terms.component';
import { ContactUsComponent } from '../global/components/contact-us/contact-us.component';
import { AboutUsComponent } from '../global/components/about-us/about-us.component';
import { TraineeResolverService } from '../trainee/services/trainee-resolver.service';
import { MeModule } from '../me/me.module';
import { TeamModule } from '../team/team.module';
const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '', redirectTo: 'me', pathMatch: 'full'
      },
      {
        path: 'terms',
        component: TermsComponent,
      },
      {
        path: 'contact-us',
        component: ContactUsComponent,
      },
      {
        path: 'about',
        component: AboutUsComponent,
      },
      {
        path: 'me',
        resolve: { 'trainee': TraineeResolverService },
        loadChildren: () => MeModule, // lazy load
        data: { preLoad: true } // preload in background navigate to Me module
      },
      {
        path: 'team',
        resolve: { 'trainee': TraineeResolverService },
        loadChildren: () => TeamModule, // lazy load
        data: { preLoad: true } // preload in background navigate to Me module
      },
    ]
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
export const routedComponents = [
  HomeComponent,
];
