import { INavLinks } from '../shared/interfaces/inav-links';
export const navLinksList: INavLinks[] = [
  {
    label: 'Me',
    route: ['me'],
    icon: '',
  },
  {
    label: 'Team',
    route: ['team'],
    icon: '',
  },
];