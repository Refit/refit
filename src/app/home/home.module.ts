// angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// components
import { HomeComponent } from './home.component';
import { routedComponents, HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../shared/shared.module';
import { UserModule } from '../user/user.module';
import { GlobalModule } from '../global/global.module';
import { TraineeService } from '../trainee/services/trainee.service';
import { TraineeResolverService } from '../trainee/services/trainee-resolver.service';
import { TraineeModule } from '../trainee/trainee.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UserModule,
    GlobalModule,
    TraineeModule,
    HomeRoutingModule,
  ],
  declarations: [
    routedComponents,
  ],
  providers: [
    TraineeService,
    TraineeResolverService,
  ],
  exports: [
    HomeComponent,
  ],
  entryComponents: [
  ]
})
export class HomeModule { }