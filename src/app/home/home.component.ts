import { Component, OnInit } from '@angular/core';
import { navLinksList } from './navigation-links';
import { INavLinks } from '../shared/interfaces/inav-links';
import { slideInAnimation } from '../shared/animation/slide-in';
import { SharedService } from '../shared/services/shared.service';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mn-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [slideInAnimation],
})
export class HomeComponent implements OnInit {
  navLinks: INavLinks[] = navLinksList;
  linkColor: string = '#000';
  animationFromLocation = {
    value: 'test', params: { perX: '100', perY: '0' }
  };
  constructor(private _sharedService: SharedService) { }
  ngOnInit() {
    // send color to subscribers via observable subject
    this._sharedService.sendMessage('#000', 'footerColor');
  }
  // get x and y location in percentages from where navigation starts
  // slide in animation
  getAnimationDirection(): any {
    return this.animationFromLocation;
  }
  menuClicked() {
    this._sharedService.menuClicked.emit();
  }
}