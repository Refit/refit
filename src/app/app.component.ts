import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationEnd, NavigationStart, NavigationCancel, NavigationError } from '@angular/router';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    loading: boolean = true; // if loading, spinner will be visible
    constructor(private _router: Router) {
        _router.events.subscribe((evt: Event) => {
            this.checkRouterEvent(evt);
        });
    }
    /////////////////////////////////////////////////////////////////////////////////
    // always scroll to top when refresgh or new route
    //////////////////////////////////////////////////////////////////////////////////
    ngOnInit() {
        // learn (routing) - scroll to top on route change
        // register a route change listener on main component and scroll to top on route changes.
        this._router.events.subscribe((evt: Event) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0);
        });
    }
    //////////////////////////////////////////////////////////////////////////////////
    // check if spinner has to be visible
    //////////////////////////////////////////////////////////////////////////////////
    checkRouterEvent(routerEvent: Event): void {
        if (routerEvent instanceof NavigationStart) {
            this.loading = true;
        } else if (routerEvent instanceof NavigationEnd ||
            routerEvent instanceof NavigationCancel ||
            routerEvent instanceof NavigationError) {
            this.loading = false;
        }
    }
}