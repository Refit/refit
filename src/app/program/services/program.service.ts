// angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
// our stuff
import { ErrorService } from '../../core/error.service';
import { IProgramExercise } from '../interfaces/iprogram-exercise';
@Injectable()
export class ProgramService {
  constructor(private _http: HttpClient, private _error: ErrorService) { }
  setSession(session: IProgramExercise[]): Observable<string> {
    // if (this.currentTrainee.traineeDetails.userName === '') {
    const obj = JSON.stringify({ session });
    return this._http.post<string>('http://localhost/Refit/php/Core/Session/AngularConnection/setSession.php',
      obj)
      .map(response => {
        // insert successful if the response
        return response;
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'setSession', null))
      );
  }
  getSession(userName: string): Observable<IProgramExercise[]> {
    const obj = JSON.stringify({ 'userName': userName });
    return this._http.post<IProgramExercise[]>('http://localhost/Refit/php/Core/Session/AngularConnection/getSession.php',
      obj)
      .map(response => {
        // insert successful if the response
        return response;
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'getTrainee', null))
      );
  }
}
