import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StartSessionComponent } from './components/start-session/start-session.component';
import { FormBuildComponent } from './components/form-build/form-build.component';
import { DoProgramComponent } from '../trainee/components/do-program/do-program.component';
import { ProgressMainComponent } from '../trainee/components/progress-main/progress-main.component';
export const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: FormBuildComponent
      },
      {
        path: 'details',
        component: FormBuildComponent
      },
      {
        path: 'build',
        component: DoProgramComponent
      },
      {
        path: 'session',
        component: StartSessionComponent
      },
      {
        path: 'progress',
        component: ProgressMainComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class ProgramRoutingModule { }