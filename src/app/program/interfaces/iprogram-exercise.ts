import { IExercise } from '../../exercises/interfaces/iexercise';
export interface IProgramExercise {
    prog_id?: number;
    exerciseName: string;
    reps: number;
    sets: number;
    rest: string;
    weight: number;
    categoryGroup: string;
    sessionDate: string;
    exercise?: IExercise;
}