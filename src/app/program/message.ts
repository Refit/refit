// messages displayed during login and signup processes
export const messageListTrainee = {
    traineeNotSent: {
        // severity: sucess, info, error, warn
        // summary - message title
        severity: 'error',
        summary: '',
        detail: 'Trainee was not sent'
    },
    traineeSent: {
        severity: 'success',
        summary: '',
        detail: 'Trainee was sent'
    },
    targetNotSent: {
        // severity: sucess, info, error, warn
        // summary - message title
        severity: 'error',
        summary: '',
        detail: 'Target was not sent'
    },
    targetSent: {
        severity: 'success',
        summary: '',
        detail: 'Target was sent'
    },
    healthConditionNotSent: {
        // severity: sucess, info, error, warn
        // summary - message title
        severity: 'error',
        summary: '',
        detail: 'Health conditions were not sent'
    },
    healthConditionSent: {
        severity: 'success',
        summary: '',
        detail: 'Health conditions were sent'
    },
    // add more messages here if required
};