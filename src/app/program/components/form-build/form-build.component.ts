import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { MenuService } from '../../../me/services/menu.service';
@Component({
  selector: 'program-form-build',
  templateUrl: './form-build.component.html',
  styleUrls: ['./form-build.component.scss']
})
export class FormBuildComponent implements OnInit {
  items: MenuItem[]; // to display Steps
  activeIndex: number = 0; // to define the curr index step
  validForm: boolean;
  constructor(private _menuService: MenuService) {
  }
  ngOnInit() {
    this.items = [{
      label: 'Personal',
      command: (event: any) => {
        this.activeIndex = 0;
      },
    },
    {
      label: 'Health',
      command: (event: any) => {
        this.activeIndex = 1;
      },
      disabled: !this._menuService.activateMenu.health
    },
    {
      label: 'Target',
      command: (event: any) => {
        this.activeIndex = 2;
      },
      disabled: !this._menuService.activateMenu.goal
    },
    {
      label: 'Summary',
      command: (event: any) => {
        this.activeIndex = 3;
      },
      disabled: !this._menuService.activateMenu.summary
    }
    ];
  }
  setValid(): void {
    this.validForm = true;
    this.items[this.activeIndex + 1].disabled = false;
  }
  checkValid(): boolean {
    return this.validForm;
  }
  nextStep(step: number): void {
    this.validForm = false;
    this.activeIndex += step;
    for (let i = 0; i <= this.activeIndex; i++) {
      this.items[i].disabled = false;
    }
  }
}