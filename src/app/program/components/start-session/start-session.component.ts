import { Component, OnInit } from '@angular/core';
import { ExercisesService } from '../../../exercises/services/exercises.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { messageList } from '../../../messages';
import { IProgramExercise } from '../../interfaces/iprogram-exercise';
import { MenuService } from '../../../me/services/menu.service';
import { ProgramService } from '../../services/program.service';
import { TraineeService } from '../../../trainee/services/trainee.service';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'program-start-session',
  templateUrl: './start-session.component.html',
  styleUrls: ['./start-session.component.scss']
})
export class StartSessionComponent implements OnInit {
  clockRestTime = { time: '', show: false };
  activateSession: boolean;
  butLabels: string[] = ['exercise name', 'category', 'reps', 'sets', 'weight', 'rest'];
  buttons: any[] = [
    { range: /^([1-9]|1[0-9]|20)$/, tip: '1-20', min: 1, max: 20 },
    { range: /^([1-9]|1[0-5])$/, tip: '1-15', min: 1, max: 15 },
    { range: /^([0-9]|[1-8][0-9]|9[0-9]|[12][0-9]{2}|300)$/, tip: '0-300', min: 0, max: 300 },
  ];
  session: IProgramExercise[];
  userName: string;
  info: string = `
  <p>Program is organized by category (legs, chest, ...) , we recommend to work by this order.</p>
  <p>Press <b>excersice name</b> button to get more information about this excersice.</p>
  <p>To update existing exercise:</p>
  <ul><li><i>In the rubrick with the number (weight, sets, reps)</i> - you can update by the narrow or writing by yourself.</li>
  <li><i>submit</i> - to send your session and save it, so you can follow your progress in <b>Progress</b>.</li>
  <li><i>category</i> - the category splits your sessions (e.g. if you can train two times a week then you have A and B : A will be one session and B the other).</li></ul>
  `;
  constructor(private _exerciseService: ExercisesService, private _messageService: MessageService,
    private _menuService: MenuService, private _programService: ProgramService,
    private _traineeService: TraineeService) { }
  //////////////////////////////////////////////////////////////////////////////////
  // ngOnInit - get exercises from the data base and sort them alphanumerically
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    this.activateSession = this._menuService.activateMenu.session;
    this.userName = this._traineeService.currentTrainee.traineeDetails.userName;
    this.getSession();
    this.getExercise();
  }
  getSession(): void {
    // clear old messages if they are still active
    console.log(this.userName);
    this._messageService.clear();
    this._programService.getSession(this.userName).subscribe(
      (data: IProgramExercise[]) => {
        if (data) {
          this.session = data;
        }
      },
      // exceptional termination of the observable sequence
      error => {
        // global message defined in app messages.ts
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }
  getExercise(): void {
    // clear old messages if they are still active
    this._messageService.clear();
  }
  public checkInput(event: any, idx: number): void {
    const pattern = this.buttons[idx].range;
    if (!pattern.test(event.target.value)) {
      // invalid character, prevent input
      event.target.value = event.target.value.replace(/[0-9]$/, '');
    }
  }
  public goToRest(ex: IProgramExercise): void {
    const rest = ex.rest;
    const saveRest = rest;
    const restTime = rest.split(':');
    let secs = +restTime[1] * 60 * 60 + +restTime[1] * 60 + +restTime[2];
    const showTime = this.clockRestTime; // closure can't access member variables
    showTime.show = true;
    function decTime() {
      secs--;
      const hours: number = Math.floor(secs / 3600);
      const minutes: number = Math.floor((secs - (hours * 3600)) / 60);
      const seconds: number = secs - (hours * 3600) - (minutes * 60);
      let strHours: string = seconds + '', strMinutes: string = minutes + '', strSecs: string = seconds + '';
      if (hours < 10) { strHours = '0' + hours; }
      if (minutes < 10) { strMinutes = '0' + minutes; }
      if (seconds < 10) { strSecs = '0' + seconds; }
      ex.rest = strHours + ':' + strMinutes + ':' + strSecs;
      showTime.time =  ex.rest;
      // check if has to be stopped
      if (secs === 0 || !showTime.show) {
        clearInterval(interval);
        ex.rest = saveRest;
        if (secs === 0) {
          this.audio = new Audio();
          this.audio.src = 'assets/sounds/bell.mp3';
          this.audio.load();
          this.audio.play();
        }
        showTime.show = false;
      }
    }
    const interval = setInterval(decTime, 1000);
  }
  public stopClock(): void {
    this.clockRestTime.show = false;
  }
  public sendSession(): void {
    this._programService.setSession(this.session).subscribe(
      (data: string) => {
        if (data) {
          this._messageService.add(messageList['wasSent']);
        }
      },
      // exceptional termination of the observable sequence
      error => {
        // global message defined in app messages.ts
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }
}