import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgramRoutingModule } from './program-routing.module';
import { PrimengModule } from '../primeng.module';
import { TraineeModule } from '../trainee/trainee.module';
import { StartSessionComponent } from './components/start-session/start-session.component';
import { FormBuildComponent } from './components/form-build/form-build.component';
import { MaterialAppModule } from '../ngmaterial.module';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ExercisesModule } from '../exercises/exercises.module';
import { ProgramService } from './services/program.service';
@NgModule({
  imports: [
    CommonModule,
    PrimengModule,
    TraineeModule,
    MaterialAppModule,
    FormsModule ,
    ProgramRoutingModule,
    SharedModule,
    ExercisesModule
  ],
  declarations: [
  StartSessionComponent,
  FormBuildComponent,
  ],
  exports: [
    StartSessionComponent,
    FormBuildComponent,
  ],
  providers: [
    ProgramService
  ]
})
export class ProgramModule { }