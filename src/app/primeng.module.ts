import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { TabViewModule } from 'primeng/tabview';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { MenubarModule } from 'primeng/menubar';
import { TabMenuModule } from 'primeng/tabmenu';
import { MenuModule } from 'primeng/menu';
import { GrowlModule } from 'primeng/growl';
import { TableModule } from 'primeng/table';
import { FileUploadModule } from 'primeng/fileupload';
import { StepsModule, Steps } from 'primeng/steps';
import { DropdownModule } from 'primeng/dropdown';
import { CardModule } from 'primeng/card';
import { CarouselModule } from 'primeng/carousel';
import { ChartModule } from 'primeng/chart';
import { OrderListModule } from 'primeng/orderlist';
import { ScheduleModule } from 'primeng/schedule';
import {KeyFilterModule} from 'primeng/keyfilter';
@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    DialogModule,
    TabViewModule,
    CodeHighlighterModule,
    MenubarModule,
    TabMenuModule,
    MenuModule,
    GrowlModule,
    TableModule,
    FileUploadModule,
    StepsModule,
    DropdownModule,
    CardModule,
    CarouselModule,
    ChartModule,
    OrderListModule,
    ScheduleModule,
    KeyFilterModule,
  ],
  declarations: [],
  exports: [
    CommonModule,
    ButtonModule,
    DialogModule,
    TabViewModule,
    CodeHighlighterModule,
    MenubarModule,
    TabMenuModule,
    MenuModule,
    GrowlModule,
    TableModule,
    FileUploadModule,
    StepsModule,
    DropdownModule,
    CardModule,
    CarouselModule,
    ChartModule,
    OrderListModule,
    ScheduleModule,
    KeyFilterModule,
  ]
})
export class PrimengModule { }
