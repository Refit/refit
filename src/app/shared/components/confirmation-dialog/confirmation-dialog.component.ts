import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sh-confirm-dialog',
  templateUrl: './confirmation-dialog.component.html',
})
// tslint:disable-next-line:component-class-suffix
export class ConfirmationDialog {
  constructor(public dialogRef: MatDialogRef<ConfirmationDialog>) {}
  confirmMessage: string;
}