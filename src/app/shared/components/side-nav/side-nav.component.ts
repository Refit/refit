import { Component, OnInit, ViewChild, HostListener, Input } from '@angular/core';
import { SharedService } from '../../services/shared.service';
import { screenSize } from '../../../core/screens';
import { INavLinks } from '../../interfaces/inav-links';
import { Router } from '@angular/router';
@Component({
  selector: 'sh-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss'],
})
export class SideNavComponent implements OnInit {
  mode = 'side';
  @ViewChild('sidenav') sidenav: any; // like getElementById
  innerWidth: number;       // width of the screen. used to set showNav
  @Input() navLinks: INavLinks[];      // links array
  //////////////////////////////////////////////////////////////////////////////////
  // this event happens when screen is resized
  //////////////////////////////////////////////////////////////////////////////////
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.setSideNavMode();
  }
  //////////////////////////////////////////////////////////////////////////////////
  // subscribe to shared event to propagate burger icon click
  //////////////////////////////////////////////////////////////////////////////////
  constructor(private _router: Router, private _sharedService: SharedService) {
    // subscribe to the event from navigation burger
    this._sharedService.menuClicked.subscribe(
      (data: any) => {
        this.sidenav.toggle();
      });
  }
  //////////////////////////////////////////////////////////////////////////////////
  // define side nav mode according to screen size
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    // set side nav mode:
    // - push/over for small device
    // - side for large device
    this.innerWidth = window.innerWidth;
    this.setSideNavMode();
  }
  //////////////////////////////////////////////////////////////////////////////////
  // set side nav mode
  //////////////////////////////////////////////////////////////////////////////////
  setSideNavMode(): void {
    if (this.innerWidth < screenSize['tablet-portrait-upper-boundary']) {
      this.mode = 'push';
      this.sidenav.close();
    } else {
      this.mode = 'side';
      this.sidenav.open();  // open side nav
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // close side nav if link was clicked and we are not in side mode
  //////////////////////////////////////////////////////////////////////////////////
  checkClose(): void {
    if (this.mode !== 'side') {
      this.sidenav.toggle();
    }
  }
}