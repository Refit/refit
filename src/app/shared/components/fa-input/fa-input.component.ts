import { Component, OnInit, Input, ContentChild, AfterContentInit, HostBinding } from '@angular/core';
import { InputRefDirective } from '../../directives/input-ref.directive';
@Component({
  selector: 'sh-fa-input',
  templateUrl: './fa-input.component.html',
  styleUrls: ['./fa-input.component.scss']
})
export class FaInputComponent implements OnInit, AfterContentInit {
  @Input() icon: string;
  // inject the inputRef directive inside the component
  @ContentChild(InputRefDirective)
  input: InputRefDirective;
  constructor() { }
  ngOnInit() { }
  ngAfterContentInit() {
    if (!this.input) {
      console.error('sh-fa-input component needs input inside its content');
    }
  }
  // link an internal property to an input property on the host element.
  // @HostBinding decorator takes one parameter, the name of the property
  // on the host element which we want to bind to.
  // in this case it is class focus
  @HostBinding('class.focus')
  // host will get the class focus equal to true or false
  // depending if this input is in focus.
  // see what happens in css file when the host is in focus (:host(.focus))
  get focus() {
    return this.input ? this.input.focus : false;
  }
  // classes variable is used in the template: [ngClass]="classes"
  // we use get here instead of simple variable since we add the logic
  // on the variable
  get classes() {
    const cssClasses = {
      'fa': true,    // class fa is active (font awesome)
      'icon': true
    };
    if (this.icon) {
      cssClasses['fa-' + this.icon] = true;
    } else {
      cssClasses['fa'] = false;
      cssClasses['icon'] = false;
    }
    return cssClasses;
  }
}