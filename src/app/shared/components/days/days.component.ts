import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
@Component({
  selector: 'sh-days',
  templateUrl: './days.component.html',
  styleUrls: ['./days.component.scss']
})
export class DaysComponent implements OnInit {
  @Input() changable: boolean = true;
  @Input() activeDays: boolean[];
  @Output() dayChange = new EventEmitter<any>(); // event to parent component
  days: string[] = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  constructor() { }
  ngOnInit() {
  }
  //////////////////////////////////////////////////////////////////////////////////
  // on day change raise event to parent component
  //////////////////////////////////////////////////////////////////////////////////
  changeDays(dayIndex: number): void {
    if (this.changable) {
      this.activeDays[dayIndex] = !this.activeDays[dayIndex];
      this.dayChange.emit(this.activeDays);
    }
  }
}