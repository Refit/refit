import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LButtonComponent } from './l-button.component';

describe('LButtonComponent', () => {
  let component: LButtonComponent;
  let fixture: ComponentFixture<LButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
