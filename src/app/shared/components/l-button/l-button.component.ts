import { Component, OnInit, Input } from '@angular/core';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sh-lbutton',
  templateUrl: './l-button.component.html',
  styleUrls: ['./l-button.component.scss']
})
export class LButtonComponent implements OnInit {
  @Input() label: string;
  constructor() { }
  ngOnInit() {
  }
}
