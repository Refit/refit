import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sh-number-input',
  templateUrl: './number-input.component.html',
  styleUrls: ['./number-input.component.scss']
})
export class NumberInputComponent implements OnInit {
  @Input() id: string;
  @Input() min: number;
  @Input() max: number;
  @Input() step: number;
  @Input() inNumber: number;
  @Input() tip: string;
  @Input() precision: number;
  @Output() changedVal = new EventEmitter<number>();
  private _value: number;
  cssClasses = {
    'error': false,
    'message': true
  };
  constructor() { }
  ngOnInit() {
    this._value = this.inNumber;
  }
  // check the value is in the valid range
  check(val: number): void {
    if (val) {
      val = +val.toFixed(this.precision);
      val = val < this.min ? this.min : val;
      val = val > this.max ? this.max : val;
      this._value = val;
      this.cssClasses.error = false;
      this.changedVal.emit(val);
    }
  }
  // getter/setter for _value
  get value(): number {
    return this._value;
  }
  set value(val: number) {
    if (val) {
      val = +val.toFixed(this.precision);
      this._value = val;
      this.cssClasses.error = false;
      if (val < this.min || val > this.max) {
        this.cssClasses.error = true;
      }
      this._value = val;
      this.changedVal.emit(val);
    }
    else {
      this.changedVal.emit(this.min - 1); // invalid value
    }
  }
  // +/= was clicked. change the value appropriately
  inc(): void {
    this._value = +this._value + +this.step;
    this.check(this._value);
  }
  dec(): void {
    this._value -= +this.step;
    this.check(this._value);
  }
}