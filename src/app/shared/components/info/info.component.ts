import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sh-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  @Input() message: string;
  @Input() icon: string = 'fa-info-circle'; // default icon
  @Input() size: string = '25'; // default size in pixels
  constructor(public dialog: MatDialog) { }
  ngOnInit() {
  }
  // classes variable is used in the template: [ngClass]="classes"
  // we use get here instead of simple variable since we add the logic
  // on the variable
  get classes() {
    const cssClasses = {
      'fa': true,
    };
    cssClasses['fa-' + this.icon] = true;
    return cssClasses;
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogInfo, {
      width: '800px',
      maxWidth: '90vw',
      data: { message: this.message }
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
@Component({
  selector: 'sh-dialog',
  styleUrls: ['./info.component.scss'],
  templateUrl: './info-sheet.html',
})
export class DialogInfo {
  @Input() header: string = 'How To';
  constructor(
    public dialogRef: MatDialogRef<DialogInfo>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
}