import { Component, OnInit, Input, ContentChild, HostBinding } from '@angular/core';
import { InputRefDirective } from '../../directives/input-ref.directive';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sh-label-input',
  templateUrl: './label-input.component.html',
  styleUrls: ['./label-input.component.scss']
})
export class LabelInputComponent implements OnInit {
  @Input() inputId: string;
  @Input() inputLabel: string;
  @Input() rtl: string = 'ltr';
  // inject the inputRef directive inside the component
  @ContentChild(InputRefDirective)
  input: InputRefDirective;
  constructor() { }
  ngOnInit() {
  }
  // link an internal property to an input property on the host element.
  // @HostBinding decorator takes one parameter, the name of the property
  // on the host element which we want to bind to.
  // in this case it is class focus
  @HostBinding('class.focus')
  // host will get the class focus equal to true or false
  // depending if this input is in focus.
  // see what happens in css file when the host is in focus (:host(.focus))
  get focus() {
    return this.input ? this.input.focus : false;
  }
}