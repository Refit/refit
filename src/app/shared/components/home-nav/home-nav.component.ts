import { Component, OnInit, Input, HostListener, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { screenSize } from '../../../core/screens';
import { INavLinks } from '../../interfaces/inav-links';
@Component({
  selector: 'sh-home-nav',
  templateUrl: './home-nav.component.html',
  styleUrls: ['./home-nav.component.scss']
})
export class HomeNavComponent implements OnInit {
  showBurger: boolean = false;  // on small screen show burger
  activeLinkIndex: number = 0;
  tabsClass: any;
  innerWidth: number;       // width of the screen. used to set showNav
  @Output() menuClicked = new EventEmitter();
  @Input() navLinks: INavLinks[];      // links array
  // default link color is black
  @Input() linkColor: string = '#000'; // link color comes from the parent (differs on various screens)
  //////////////////////////////////////////////////////////////////////////////////
  // screen resize
  //////////////////////////////////////////////////////////////////////////////////
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.setShowBurger();
    this.setActiveClass(this.activeLinkIndex);
  }
  constructor(private _router: Router) { }
  //////////////////////////////////////////////////////////////////////////////////
  // set navigation according to the screen size
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.setShowBurger();
    this.setActiveClass(0);
  }
  //////////////////////////////////////////////////////////////////////////////////
  // the following functions solves angular material tab bug,
  // which doesn't set properly the active route class
  //////////////////////////////////////////////////////////////////////////////////
  setActiveClass(indexOfRouteLink: number) {
    this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab =>
      this._router.url.split('/').indexOf(tab.route[0].split('/').pop()) !== -1
    ));
    this.tabsClass = { 'mat-tab-label-active': 'false' };
    if (this.activeLinkIndex === indexOfRouteLink) {
      this.tabsClass = { 'mat-tab-label-active': 'true' };
    }
    return this.tabsClass;
  }
  //////////////////////////////////////////////////////////////////////////////////
  // choose between full nav and burger nav on small screens
  //////////////////////////////////////////////////////////////////////////////////
  setShowBurger(): void {
    if (this.innerWidth < screenSize['tablet-portrait-upper-boundary']) {
      this.showBurger = true;
    } else {
      this.showBurger = false;
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // transfer event to parent when burger menu was clicked
  //////////////////////////////////////////////////////////////////////////////////
  menuClick(): void {
    this.menuClicked.emit();
  }
}