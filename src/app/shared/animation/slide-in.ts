// import the required animation functions from the angular animations module
import { trigger, animate, transition, style } from '@angular/animations';
export const slideInAnimation =
  // trigger name for attaching this animation to an element using the [@triggerName] syntax
  trigger('slideInAnimation', [
    // route 'enter' transition
    transition(':enter', [
      style({ transform: 'translate( {{perX}}%, {{perY}}% )' }),
      animate('{{time}}s ease-in-out', style({ transform: 'translate(0%, 0%)' }))
    // default parameters
    ], {params : { perX: '100', perY: '0' , time: '0.2'}}),
  ]);

