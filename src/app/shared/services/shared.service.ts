import { Injectable, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class SharedService {
  @Output() menuClicked = new EventEmitter();
  //////////////////////////////////////////////////////////////////////////////////
  // component - component communication through message
  //////////////////////////////////////////////////////////////////////////////////
  // communicating between components with Observable & Subject
  private message = new Subject<any>();
  sendMessage(message: string, type: string) {
      const messageObj: { [key: string]: string } = {};
      messageObj[type] = message;
      this.message.next(messageObj);
  }
  clearMessage() {
      this.message.next();
  }
  getMessage(): Observable<any> {
      return this.message.asObservable();
  }
}