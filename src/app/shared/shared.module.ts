import { NgModule } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PrimengModule } from '../primeng.module';
import { MaterialAppModule } from '../ngmaterial.module';
import { FaInputComponent } from './components/fa-input/fa-input.component';
import { InputRefDirective } from './directives/input-ref.directive';
import { NavComponent } from './components/nav/nav.component';
import { LogoComponent } from './components/logo/logo.component';
import { LabelInputComponent } from './components/label-input/label-input.component';
import { InfoComponent, DialogInfo } from './components/info/info.component';
import { LButtonComponent } from './components/l-button/l-button.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { HomeNavComponent } from './components/home-nav/home-nav.component';
import {ScheduleModule} from 'primeng/schedule';
import { FormsModule } from '@angular/forms';
import { ConfirmationDialog } from './components/confirmation-dialog/confirmation-dialog.component';
import { NumberInputComponent } from './components/number-input/number-input.component';
import { DaysComponent } from './components/days/days.component';
@NgModule({
  imports: [
    CommonModule,
    PrimengModule,
    MaterialAppModule,
    FormsModule,
    ScheduleModule,
  ],
  declarations: [
    FaInputComponent,
    InputRefDirective,
    NavComponent,
    SideNavComponent,
    HomeNavComponent,
    LogoComponent,
    LabelInputComponent,
    InfoComponent,
    DialogInfo,
    LButtonComponent,
    HomeNavComponent,
    ConfirmationDialog,
    NumberInputComponent,
    DaysComponent,
  ],
  providers: [
  ],
  exports: [
    CommonModule,
    FaInputComponent,
    InputRefDirective,
    NavComponent,
    SideNavComponent,
    HomeNavComponent,
    LogoComponent,
    LabelInputComponent,
    InfoComponent,
    DialogInfo,
    LButtonComponent,
    ConfirmationDialog,
    NumberInputComponent,
    DaysComponent,
    PrimengModule,
    MaterialAppModule,
  ],
  entryComponents: [
    DialogInfo,
    ConfirmationDialog,
  ]
})
export class SharedModule { }