export interface INavLinks {
    label: string;
    route: string[];
    icon: string;
}