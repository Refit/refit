import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { MessageService } from 'primeng/components/common/messageservice';

@Injectable()
export class ErrorService {

  constructor(private _messageService: MessageService) { }

  handleHttpError<T>(serviceName = '', operation = 'operation', result = {} as T) {
    return (error: HttpErrorResponse): Observable<T> => {
      const message = (error.error instanceof ErrorEvent) ?
        error.error.message : `server returned code ${error.status}`;
        // `server returned code ${error.status} with body "${error.error}"`;

      // throw error message using primeng message service
      this._messageService.add({severity: 'info', summary: '',
          detail: `${operation} failed: ${message}`});
      // Let the app keep running by returning a safe result.
      return of(result);
    };
  }
}
