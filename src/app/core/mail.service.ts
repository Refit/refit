import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ErrorService } from './error.service';

@Injectable()
export class MailService {
    constructor(private _http: HttpClient, private _error: ErrorService) { }
    sendEmail(email: string, message: string, name: string): Observable<any> {
        const obj = JSON.stringify({ 'email': email, 'message': message, 'name': name });
        //  console.log(obj);
        return this._http.post<any>('http://localhost/Refit/php/Core/User/AngularConnection/contactUsMail.php',
            obj)
            .map(response => {
                // sugnup successful if the response
                console.log('HTTP response is ' + response);
                return response;
            })
            .pipe(
                // http error
                catchError(this._error.handleHttpError('', 'send-email', []))
            );
    }
}
