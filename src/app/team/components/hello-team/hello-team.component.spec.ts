import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelloTeamComponent } from './hello-team.component';

describe('HelloTeamComponent', () => {
  let component: HelloTeamComponent;
  let fixture: ComponentFixture<HelloTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelloTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelloTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
