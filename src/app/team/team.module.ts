import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routedComponents, TeamRoutingModule } from './team-routing.module';
@NgModule({
  imports: [
    CommonModule,
    TeamRoutingModule
  ],
  declarations: [
    routedComponents,
  ],
})
export class TeamModule { }