import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramModule } from '../program/program.module';
import { CompetitionMainComponent } from './components/competition-main/competition-main.component';
import { HelloTeamComponent } from './components/hello-team/hello-team.component';
export const routes: Routes = [
  {
    path: '',
    component: CompetitionMainComponent,
    children: [
      {
        path: '', redirectTo: 'hello', pathMatch: 'full'
      },
      {
        path: 'hello',
        component: HelloTeamComponent,
      },
      {
        path: 'program',
        loadChildren: () => ProgramModule, // lazy load
        data: { preload: true }  // preload in background to be ready to use when user navigates to home
      }
    ]
  },
];
@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class TeamRoutingModule { }

export const routedComponents = [
  CompetitionMainComponent,
  HelloTeamComponent
];