import { INavLinks } from '../../shared/interfaces/inav-links';

export const navLinksList: INavLinks[] = [
      {
        label: 'About',
        route: ['/about'],
        icon: '',
      },
      {
        label: 'Contact Us',
        route: ['/contact-us'],
        icon: '',
      },
      {
        label: 'Terms',
        route: ['/terms'],
        icon: '',
      },
];

export const icons: INavLinks[] = [
  {
    label: '',
    route: [],
    icon: 'fa fa-facebook-f',
  },
  {
    label: '',
    route: [],
    icon: 'fa fa-instagram',
  },
  {
    label: '',
    route: ['https://www.google.com'],
    icon: 'fa fa-google',
  },
];


