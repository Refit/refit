import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { SharedService } from '../../shared/services/shared.service';
import { navLinksList, icons } from './navigation-links';
import { INavLinks } from '../../shared/interfaces/inav-links';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy {
  // learn (Componene communication, Message, Observable) -
  // how to receive message from other components
  // in our case we receive key: value pair,
  // but message sent can be also an object (if defined so in the send function)
  subscription: Subscription;
  links: INavLinks[] = navLinksList;
  color: string = '#000';
  bgColor: string = 'transparent';
  homePath: string = 'welcome/';
  mediaIcons: INavLinks[] = icons;

  constructor(private _sharedService: SharedService, private _router: Router) {
    // subscribe to messages passed through the shared service.
    // in our case the message is the footer color. it is sent on
    // welcome/admin/home ngOnInit (look there)
    this.subscription = this._sharedService.getMessage().subscribe(message => {
      this.color = message && message['footerColor'] ? message['footerColor'] : this.color;
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

  getColor() {
    return this.color;
  }
  getBgColor() {
    if (this.color === '#000') {
      this.bgColor = '#ddd';
    } else {
      this.bgColor = 'transparent';
    }
    return this.bgColor;
  }


  //////////////////////////////////////////////////////////////////////////////////
  // go outside of this site (by media icons)
  //////////////////////////////////////////////////////////////////////////////////
  // learn (routing) - how to go to link outside this site
  goToSpecificUrl(url): void {

    // window.location.href = url;

    // add http prefix if doesn't exist in order to open the link properly
    let urlPrefix: string = '';
    if (!/^http[s]?:\/\//.test(url)) {
      urlPrefix += 'http://';
    }

    url = urlPrefix + url;
    window.open(url, '_blank');
  }

  //////////////////////////////////////////////////////////////////////////////////
  // go to the site link according the module we are currently there,
  // thus we we are in admin, the link will be under admin, home - under home,
  // welcome - under welcome
  //////////////////////////////////////////////////////////////////////////////////
  goToLink(link) {
    const href = this._router.url.split('/')[1] + link;
    this._router.navigateByUrl('/' + href);
  }
}


