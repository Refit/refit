import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../user/services/auth.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {
  constructor(private _router: Router, private _authService: AuthService) { }
  ngOnInit() {
  }
  //////////////////////////////////////////////////////////////////////////////////
  // go to home page on pressing x icon
  //////////////////////////////////////////////////////////////////////////////////
  goHome() {
    this._authService.logout();
    this._router.navigateByUrl('/home');
  }
}
