import { Component, OnInit } from '@angular/core';
import { navLinksList } from './navigation-links';
import { INavLinks } from '../../shared/interfaces/inav-links';
import { SharedService } from '../../shared/services/shared.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  navLinks: INavLinks[] = navLinksList;
  linkColor: string = '#fff';
  coverClasses = {
    'cover': true,
  };
  constructor(private _sharedService: SharedService) { }
  ngOnInit() {
    // send color to subscribers via observable subject
    // this._sharedService.clearMessage();
    this._sharedService.sendMessage('#fff', 'footerColor');
  }
}
