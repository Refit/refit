// messages displayed during login and signup processes
export const messageList = {
    fileExistsUploadError: {
        // severity: sucess, info, error, warn
        // summary - message title
        severity: 'error',
        summary: '',
        detail: 'file already exists'
    },
    fileTypeUploadError: {
        severity: 'error',
        summary: '',
        detail: 'only JPG, JPEG, PNG & GIF files are allowed'
    },
    fileUploadGenericError: {
        severity: 'error',
        summary: '',
        detail: 'there was an error uploading your file'
    },
    fileUploadSuccess: {
        severity: 'sucess',
        summary: '',
        detail: 'file uploaded succesfully'
    },
    fakeImageUploadError: {
        severity: 'sucess',
        summary: '',
        detail: 'file is not an image'
    },
};