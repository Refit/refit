import { Component, OnInit, Input , Inject} from '@angular/core';
import { IExercise } from '../../interfaces/iexercise';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pr-ex',
  templateUrl: './ex.component.html',
  styleUrls: ['./ex.component.scss']
})
export class ExComponent implements OnInit {
  display: boolean = false;
  private _exercise: IExercise;
  @Input() set exercise(ex: IExercise) {
    this._exercise = ex;
    // console.log(this._exercise);
  }
  get exercise(): IExercise {
    return this._exercise;
  }
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(ExDialog, {
      width: '900px',
      maxWidth: '90vw',
      data: this.exercise
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ex-dialog',
  templateUrl: './ex.dialog.html',
  styleUrls: ['./ex.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class ExDialog {
  constructor(
    public dialogRef: MatDialogRef<ExDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
