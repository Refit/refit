import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExChangeFormComponent } from './ex-change-form.component';

describe('ExChangeFormComponent', () => {
  let component: ExChangeFormComponent;
  let fixture: ComponentFixture<ExChangeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExChangeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExChangeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
