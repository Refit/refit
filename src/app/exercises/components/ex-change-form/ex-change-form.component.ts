// angular
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
// libraries
import { SelectItem } from 'primeng/api';
import { FileUpload } from 'primeng/fileupload';
import { MessageService } from 'primeng/components/common/messageservice';
// our stuff
import { IExercise, emptyExercise, categoryList } from '../../interfaces/iexercise';
import { ExercisesService } from '../../services/exercises.service';
import { messageList } from '../../messages';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ex-change-form',
  templateUrl: './ex-change-form.component.html',
  styleUrls: ['./ex-change-form.component.scss']
})
export class ExChangeFormComponent implements OnInit {
  @Input() ex: IExercise = emptyExercise;
  @Output() submitEx = new EventEmitter<IExercise>();
  @Input() isEdit: boolean;
  complex: number[];
  health: number[];
  categories: string[] = categoryList;
  currCategory: string;
  uploadedFiles: any[] = [];
  FileUploaded: boolean;
  @ViewChild('fileInput') fileInput: FileUpload;
  categoriesSelect: SelectItem[];
  constructor(private _exercisesService: ExercisesService, private _messageService: MessageService, private _location: Location) {}
  ngOnInit() {
    this.health = [1, 2, 3, 4];
    this.complex = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    if (this.ex.exerciseName === '') {
      this.isEdit = false;
    } else {
      this.isEdit = true;
      this.currCategory = this.ex.category;
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // go to previous page
  //////////////////////////////////////////////////////////////////////////////////
  goBack() {
    this._location.back();
  }
  //////////////////////////////////////////////////////////////////////////////////
  // on form submit raise event to parent component
  // on clear - clear form fields. take care of exercise name in case of update
  //////////////////////////////////////////////////////////////////////////////////
  public onSubmit(): void {

      this.startUpload();
        setTimeout(() => {  this.submitEx.emit(this.ex); }, 2000);
      
  }
  clearForm(): void {
    const exName = this.ex.exerciseName;
    this.ex = JSON.parse(JSON.stringify(emptyExercise));
    if (this.isEdit) {
      this.ex.exerciseName = exName;
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // handle picture file upload
  //////////////////////////////////////////////////////////////////////////////////
  startUpload(): void {
    
    this.fileInput.upload();
  }
  // Callback to invoke when file upload is complete.
  onUpload(event): void {
    // clear old messages if they are still active
    this._messageService.clear();
    for (const file of event.files) {
      this.uploadedFiles.push(file);
    }
    if(this.uploadedFiles[0].name !== null){
      this.ex.picture = "assets/images/" + this.uploadedFiles[0].name;
    }

    const response = event.xhr.response; // response from the server
    switch (response) {
      case 'file already exists':
        this._messageService.add(messageList['fileExistsUploadError']);
        break;
      case 'only JPG, JPEG, PNG & GIF files are allowed':
        this._messageService.add(messageList['fileTypeUploadError']);

        break;
      case 'there was an error uploading your file':
        this._messageService.add(messageList['fileUploadGenericError']);
        break;
      case 'file is not an image':
        this._messageService.add(messageList['fakeImageUploadError']);
        break;
      default:
      
        this._messageService.add(messageList['fileUploadSuccess']);
    }
  }
  // Callback to invoke if file upload fails.
  onError(event): void {
    this._messageService.add(messageList['fileUploadGenericError']);
  }

}
