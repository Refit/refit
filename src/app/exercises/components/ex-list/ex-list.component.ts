import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ICat } from '../../interfaces/icat-exercises';
import { IExercise } from '../../interfaces/iexercise';
import { ExercisesService } from '../../services/exercises.service';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ex-list',
  templateUrl: './ex-list.component.html',
  styleUrls: ['./ex-list.component.scss']
})
export class ExListComponent implements OnInit {
  panelOpenState: boolean = false;
  private _exercises: ICat[];
  filtered: IExercise[] = [];   // filtered users
  private _filterBy: string = ''; // filter string
  @Input() info: string = '';
  get filterBy(): string {
    return this._filterBy;
  }
  set filterBy(val: string) {
    this._filterBy = val;
    if (this._filterBy === '') {
      this.filtered = [];   // clear display
    }
  }
  @Input() set exercises(ex: ICat[]) {
    this._exercises = ex;
    // console.log(this._exercises);
  }
  get exercises(): ICat[] {
    return this._exercises;
  }
  @Output() removeExEvent = new EventEmitter<IExercise>();
  @Output() addExEvent = new EventEmitter<ICat>();
  @Output() editExEvent = new EventEmitter<IExercise>();
  expanded: { [cat: string]: boolean } = {};
  constructor(private _exerciseService: ExercisesService) { }
  ngOnInit() {
    // define if panel has to be expanded
    const expandedCategory = this._exerciseService.expandedPanelExList;
    if (this._exercises) {
      for (let i = 0; i < this._exercises.length; i++) {
        this.expanded[this._exercises[i].category] = false;
        if (this._exercises[i].category === expandedCategory) {
          this.expanded[this._exercises[i].category] = true;
        }
      }
    }
  }
  //////////////////////////////////////////////////////////////////////////////////
  // define that panel is expanded/collapsed, thus on go back to this page,
  // it will stay in expanded state
  //////////////////////////////////////////////////////////////////////////////////
  setExpanded(ex: ICat): void {
    // name of category, which panel is expanded in display
    this._exerciseService.expandedPanelExList = ex.category;
  }
  collapseExpanded() {
    // all panels are collapsed - currently not used
    this._exerciseService.expandedPanelExList = '';
  }
  //////////////////////////////////////////////////////////////////////////////////
  // transfer events to parent component
  //////////////////////////////////////////////////////////////////////////////////
  addtoObject(ex: ICat): void {
    this.addExEvent.emit(ex);
  }
  removeEx(ex: IExercise): void {
    this.removeExEvent.emit(ex);
  }
  editEx(ex: IExercise): void {
    this.editExEvent.emit(ex);
  }
  //////////////////////////////////////////////////////////////////////////////////
  // set bugs by filterBy owner name. the filtered bugs will be shown
  // under the owner name
  //////////////////////////////////////////////////////////////////////////////////
  updateFilter(): void {
    const filterBy: string = this._filterBy.toLowerCase();
    this.filtered = [];
    if (filterBy !== '') {
      this.collapseExpanded(); // only filtered panel will be expanded
      this.exercises.forEach(element => {
        const exs = element.exercises;
        exs.forEach(ex => {
          if (ex.exerciseName.toLocaleLowerCase().indexOf(filterBy) === 0) {
            this.filtered.push(ex);
          }
        });
        this.filtered.sort((a, b) => {
          if (a.exerciseName > b.exerciseName) {
            return 1;
          }
          if (a.exerciseName < b.exerciseName) {
            return -1;
          }
          return 0;
        });
      });
    }
  }
}
