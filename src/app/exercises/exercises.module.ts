import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExListComponent } from './components/ex-list/ex-list.component';
import { ExComponent, ExDialog } from './components/ex/ex.component';
import { ExercisesService } from './services/exercises.service';
import { ExChangeFormComponent } from './components/ex-change-form/ex-change-form.component';
import { FormsModule } from '@angular/forms';
import { MaterialAppModule } from '../ngmaterial.module';
import { PrimengModule } from '../primeng.module';
import { MatDialogModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule ,
    MaterialAppModule ,
    PrimengModule ,
    MatDialogModule,
    SharedModule,
  ],
  declarations: [
    ExListComponent,
    ExComponent,
    ExChangeFormComponent,
    ExDialog
  ],
  exports: [
    ExListComponent,
    ExComponent,
    ExChangeFormComponent,
    ExDialog ,
  ],
  providers: [
    ExercisesService,
  ],
  entryComponents: [ExComponent , ExDialog ],
})
export class ExercisesModule { }