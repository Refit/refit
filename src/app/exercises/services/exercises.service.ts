import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { catchError, tap, retry } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ErrorService } from '../../core/error.service';
import { ICat } from '../interfaces/icat-exercises';
import { IExercise } from '../interfaces/iexercise';
@Injectable()
export class ExercisesService {
  allExercises: ICat[];
  currentEx: IExercise;
  expandedPanelExList: string = ''; // category panel to be expanded
  constructor(private _http: HttpClient, private _error: ErrorService) { }
  //////////////////////////////////////////////////////////////////////////////////
  // update exercise to the data base through http and also update the local data base
  //////////////////////////////////////////////////////////////////////////////////
  editEx(ex: IExercise): Observable<any> {
    // update the local exercise data base
    if (this.allExercises) {
      const cat: string = ex.category.toLowerCase();
      const exs: IExercise[] = this.allExercises.find(x => x.category.toLowerCase() === cat).exercises;
      exs.forEach((item, index) => {
        if (item === ex) {
          exs[index] = ex;
        }
      });
    }
    const obj = JSON.stringify(ex);
    return this._http.post<any>('http://localhost/Refit/php/Core/Exercise/AngularConnection/editEx.php',
      obj).map(response => {
        // added successful if the response
        // console.log('HTTP response is ' + response);
        // console.log(ex);
        return response;
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'editEx', []))
      );
  }
  //////////////////////////////////////////////////////////////////////////////////
  // remove exercise to the data base through http and also update the local data base
  //////////////////////////////////////////////////////////////////////////////////
  removeEx(ex: IExercise): Observable<any> {
    // update the local exercise data base
    if (this.allExercises) {
      const cat: string = ex.category.toLowerCase();
      const exs: IExercise[] = this.allExercises.find(x => x.category.toLowerCase() === cat).exercises;
      exs.forEach((item, index) => {
        if (item === ex) {
          exs.splice(index, 1);
        }
      });
    }
    const obj = JSON.stringify(ex.exerciseName);
    return this._http.post<any>('http://localhost/Refit/php/Core/Exercise/AngularConnection/removeEx.php',
      obj)
      .map(response => {
        // added successful if the response
        return response;
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'removeEx', []))
      );
  }
  //////////////////////////////////////////////////////////////////////////////////
  // add exercise to the data base through http and also update the local data base
  //////////////////////////////////////////////////////////////////////////////////
  addEx(ex: IExercise): Observable<any> {
    const obj = JSON.stringify(ex);
    // update the local exercise data base
    if (this.allExercises) {
      const cat: string = ex.category.toLowerCase();
      this.allExercises.find(x => x.category.toLowerCase() === cat).exercises.push(ex);
    }
    return this._http.post<any>('http://localhost/Refit/php/Core/Exercise/AngularConnection/addEx.php',
      obj)
      .map(response => {
        // sugnup successful if the response
        console.log('HTTP response is ' + response);
        return response;
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'addEx', []))
      );
  }
  //////////////////////////////////////////////////////////////////////////////////
  // get all exercises from the data base on page refresh.
  // otherwise, extract the exercises from the local data base this.allExercises
  // in order to save the transfer time overhead
  //////////////////////////////////////////////////////////////////////////////////
  getExercises(): Observable<ICat[]> {
    if (this.allExercises) {
      return of(this.allExercises);
    }
    return this._http.get<ICat[]>(
      'http://localhost/Refit/php/Core/Exercise/AngularConnection/getAllExercises.php')
      .pipe(
        retry(3), // retry a failed request up to 3 times
        tap(data => this.allExercises = data),
        catchError(this._error.handleHttpError('', 'getExercises', []))
      );
  }

  getExercise(exerciseName: string): Observable<any> {
    console.log(exerciseName);
    const obj = JSON.stringify({ 'exerciseName': exerciseName });
    return this._http.post<any>('http://localhost/Refit/php/Core/Exercise/AngularConnection/getEx.php',
      obj)
      .map(response => {
        // insert successful if the response
        return response;
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'getTrainee', null))
      );
  }

  addExImage(exName: string , ImageName: String): Observable<any> {
    const obj = JSON.stringify( { 'exerciseName': exName , 'ImageName': ImageName });
    return this._http.post<any>('http://localhost/Refit/php/Core/Exercise/AngularConnection/ConnectImageEX.php',
      obj)
      .map(response => {
        // sugnup successful if the response
        console.log('HTTP response is ' + response);
        return response;
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'ConnectImageEX', []))
      );
  }
}