import { IExercise } from './iexercise';
export interface ICat {
    category: string;
    exercises: IExercise[];
}