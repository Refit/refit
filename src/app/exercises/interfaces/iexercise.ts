export interface IExercise {
    exerciseName: string;
    category: string;
    exerciseType: string;
    instructions: string;
    video: string;
    picture: string;
    recommended: number;
    mistakes: string;
    activityGroup: string;
    complexMin: number;
    complexMax: number;
    health: number;
    motivation: number;
    isFast: number;
}
export const emptyExercise: IExercise = {
    exerciseName: '',
    category: 'legs',
    exerciseType: '',
    instructions: `
<p class="type">Difficulty:</p><p>Intermediate</p>
<p class="type">Description:</p>
<p>Write exercise description here</p>
<p class="type">How to do it:</p>
<p>Do this exercise in the following steps</p>
<ol>
<li>Stand tall with your feet hip distance apart. Your hips, knees, and toes should all be facing forward.</li>

<li>Bend your knees and extend your buttocks backward as if you are going to sit back into a chair. Make sure that you keep your knees behind your toes and your weight in your heels. Rise back up and repeat.</li>
</ol>

<p class="type">Benefits:</p>
<p>The squat is an extremely effective lower body move that strengthens all leg muscles including gluts, quads, hamstrings and calves. During the squat you use every single lower body muscle, the motion of keeping your balance all while maintaining an upright posture will deliver an entire leg workout with core strength as an added bonus making the squat the ultimate lower body workout.</p>
<p>Squats also help strengthen your back when you practice good posture in your squat and pull your abdominal muscles in throughout. Squat can also reduce future injuries by strengthening both the knee and ankle muscles.</p>
<p class="type">Comments:</p>
<p>Write comments here</p>
        `,
    video: 'No video',
    picture: 'No picture',
    recommended: 0,
    mistakes: 'No mistakes',
    activityGroup: 'Activity Group unknown',
    complexMin: -1,
    complexMax: -1,
    health: -1,
    motivation: 1,
    isFast: 1
};
export const categoryList: string[] = [
    'legs',
    'chest',
    'shoulders',
    'back',
    'biceps',
    'triceps',
    'abdominals',
];
