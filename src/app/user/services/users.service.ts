import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { catchError, tap, retry } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from '../../core/error.service';
import { IUser } from '../interfaces/iuser';
@Injectable()
export class UsersService {
  currentUser: IUser;
  users: IUser[];
  expandedPanelUserList: string = ''; // category panel to be expanded
  filterBy: string = ''; // for user list filter
  constructor(private _http: HttpClient, private _error: ErrorService) { }
  removeUser(name: string): Observable<any> {
    const obj = JSON.stringify(name);
    return this._http.post<any>('http://localhost/Refit/php/Core/User/AngularConnection/removeUser.php',
      obj).map(response => {
        return response;
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'removeUser', []))
      );
  }
  editUser(user: IUser): Observable<any> {
    const obj = JSON.stringify(user);
    return this._http.post<IUser>('http://localhost/Refit/php/Core/User/AngularConnection/editUser.php',
      obj).map(response => {
        // successful if the response
        return response;
      })
      .pipe(
        // http error
        catchError(this._error.handleHttpError('', 'editUser', []))
      );
  }
  getUsers(): Observable<IUser[]> {
    if (this.users) {
      return of(this.users);
    }
    return this._http.get<IUser[]>(
      'http://localhost/Refit/php/Core/User/AngularConnection/getUsers.php')
      .pipe(
        retry(3), // retry a failed request up to 3 times
        tap(data => this.users = data),
        catchError(this._error.handleHttpError('', 'getUsers', []))
      );
  }
}