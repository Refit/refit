import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router, Route, CanLoad } from '@angular/router';
import { AuthService } from './auth.service';
// CanActivate Guard for routes in the site - used in routing module
@Injectable()
export class AuthGuardService implements CanActivate , CanLoad {
    constructor(private _authService: AuthService,
                private _router: Router) { }
    // used in routing to active the route if user is checkd in
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.checkLoggedIn(state.url);
    }
    // used in routing to load lazy module
    canLoad(route: Route): boolean {
        return this.checkLoggedIn(route.path);
    }
    checkLoggedIn(url: string): boolean {
        if (this._authService.isLoggedIn()) {
            return true;
        }
        this._authService.redirectUrl = url;
        this._router.navigateByUrl('/welcome');
        return false;
    }
}