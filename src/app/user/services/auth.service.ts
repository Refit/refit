import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { IUser } from '../interfaces/iuser';
import { ErrorService } from '../../core/error.service';
import { UsersService } from './users.service';
@Injectable()
export class AuthService implements OnInit {
    currentUser: IUser = null;
    redirectUrl: string;
    isAuthenticated: boolean = false;
    constructor(private _http: HttpClient, private _error: ErrorService, private _userService: UsersService) { }
    ngOnInit() {}
    //////////////////////////////////////////////////////////////////////////////////
    // signup
    //////////////////////////////////////////////////////////////////////////////////
    signup(user: IUser): Observable<string | any> {
        const obj = JSON.stringify(user);
        return this._http.post<string>('http://localhost/Refit/php/Core/User/AngularConnection/signup.php',
            obj)
            .map(response => {
                // signup successful if the response
                user.id = 9999;
                user.isAdmin = 'U';
                return response;
            })
            .pipe(
                // http error
                catchError(this._error.handleHttpError('', 'signup', []))
            );
    }

    //////////////////////////////////////////////////////////////////////////////////
    // login
    //////////////////////////////////////////////////////////////////////////////////
    login(user: IUser, rememberMe: boolean): Observable<string | any> {
        this.currentUser = user;
        const obj = JSON.stringify(user);
        return this._http.post<string>('http://localhost/Refit/php/Core/User/AngularConnection/checkUser.php',
            obj).map(response => {
                // login successful if the response
                // store user details in local storage to keep user logged in between page refreshes
                if (response !== 'error') {
                    this.currentUser.isAdmin = response;
                    if (rememberMe) {
                        // transforms JSON object into a JSON string and store in local storage
                        localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
                        localStorage.setItem('rememberMe', 'remember');
                    } else {
                        localStorage.removeItem('currentUser');
                        localStorage.setItem('rememberMe', '');
                    }
                    // user authentified
                    this.isAuthenticated = true;
                    return response;
                }
                return null;
            })
            .pipe(
                // http error
                catchError(this._error.handleHttpError('', 'login', []))
            );
    }
    //////////////////////////////////////////////////////////////////////////////////
    // logout
    //////////////////////////////////////////////////////////////////////////////////
    logout(): void {
        this.currentUser = null;
        this.isAuthenticated = false;
        // remove the user from the local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('rememberMe');
    }
    //////////////////////////////////////////////////////////////////////////////////
    // check if user was already logged in for authentification
    //////////////////////////////////////////////////////////////////////////////////
    isLoggedIn(): boolean {
        // get saved data in the local storage
        const user = localStorage.getItem('currentUser');
        const remember = localStorage.getItem('rememberMe');
        if (remember !== null && remember !== '') {
            // we are here if it is not the first time login
            if (user === null) {
                // something was wrong?
                this.isAuthenticated = false;
            } else {
                // not the first time with remember and user data
                this.isAuthenticated = true;
                // set user data
                this.currentUser = JSON.parse(user);
            }
        }
        // else it is first time in the system or after logout
        return this.isAuthenticated;
    }
    //////////////////////////////////////////////////////////////////////////////////
    // check if user is admin
    //////////////////////////////////////////////////////////////////////////////////
    isAdmin() {
        return this.currentUser.isAdmin === 'A';
    }
}