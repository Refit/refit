import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialAppModule } from '../ngmaterial.module';
import { PrimengModule } from '../primeng.module';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { UserRoutingModule } from './user-routing.module';
import { SharedModule } from '../shared/shared.module';
import { UsersService } from './services/users.service';
import { UserUpdateComponent, UserUpdateDialog } from './components/user-update/user-update.component';
import { UserListComponent } from './components/user-list/user-list.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    MaterialAppModule,
    PrimengModule,

    SharedModule,
    UserRoutingModule,
  ],
  declarations: [
    LoginComponent,
    LogoutComponent,
    SignUpComponent,
    UserListComponent,
    UserUpdateComponent,
    UserUpdateComponent,
    UserUpdateDialog,
  ],
  exports: [
    LogoutComponent,
    UserListComponent,
    UserUpdateComponent,
    UserUpdateDialog,
  ],
  providers: [
    UsersService
  ],
  // The entryComponents array is used to define only components that are not found in html
  // and created dynamically
  entryComponents: [
    UserUpdateComponent,
    UserUpdateDialog
  ],
})
export class UserModule { }