// messages displayed during login and signup processes
export const messageList = {
        userNotfound: {
            // severity: sucess, info, error, warn
            // summary - message title
            severity: 'error',
            summary: '',
            detail: 'User not found, please register'
        },
        userExists:
        {
            severity: 'error',
            summary: '',
            detail: 'User already exist, please register again'
        },
        invalidTermination:
        {
            severity: 'error',
            summary: '',
            detail: 'operation failed'
        }
        // add more messages here if required
    }
;