import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'user-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {
  constructor(public authService: AuthService , private _router: Router) {  }
  ngOnInit() { }
  logout(): void {
    this.authService.logout();
    // navigateByUrl and not navigate to remove all extra routing parameters if where defined previously
    this._router.navigateByUrl('/welcome');
  }
}