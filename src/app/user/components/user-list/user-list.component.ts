import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { IUser } from '../../interfaces/iuser';
import { UsersService } from '../../services/users.service';
interface usersDisplay {
  [group: string]: IUser[];
}
@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  // information to be displayed when pressing the info icon
  info: string = `
    <p>Users list is ordered in groups in Alphabetical order.
    <p>To see existing users, first press the <b>group</b>.
    Then press:</p>
    <ul><li><i>edit</i> - to update the user</li>
    <li><i>delete</i> - to remove the user from the list</li></ul>
    <p>You can write a <b>string</b> (case insensitive) in the input box
    to get all users, which names start with the string you wrote.
    To <b>clear</b> the list, just empty the input box.</p>
    `;
  // groups for users display
  groupList: string[] = [
    'A-C', 'D-G', 'H-L', 'M-P', 'Q-T', 'U-Z'
  ];
  range: string[] = [
    'a', 'd', 'h', 'm', 'q', 'u'
  ];
  filtered: IUser[] = [];   // filtered users
  private _filterBy: string = ''; // filter string
  get filterBy(): string {
    return this._filterBy;
  }
  set filterBy(val: string) {
    this._filterBy = val;
    if (this._filterBy === '') {
      this.filtered = [];   // clear display
    }
  }
  userShow: usersDisplay = {};  // users organized for display
  private _users: IUser[];    // all users
  @Input() set users(users: IUser[]) {
    this._users = users;
    this.sortData();
    this.updateFilter();
  }
  get users() {
    return this._users;
  }
  // event passed to parent component
  @Output() removeUserEvent = new EventEmitter<string>();
  @Output() editUserEvent = new EventEmitter<IUser>();
  expanded: { [range: string]: boolean } = {};
  constructor(private _userService: UsersService) { }
  /////////////////////////////////////////////////////////////////////////////////
  // ngOnInit - set last expandedpanel and filter (currently not used)
  // sort user list
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    this.sortData();
    // define if panel has to be expanded
    const expandedUserList = this._userService.expandedPanelUserList;
    if (this._users) {
      this.groupList.forEach(element => {
        this.expanded[element] = element === expandedUserList;
      });
    }
    this._filterBy = this._userService.filterBy;
    if (this._filterBy !== '') {
      this.updateFilter();
    }
  }
    //////////////////////////////////////////////////////////////////////////////////
    // set panel as expanded/collapsed, thus on go back to this page,
    // it will stay in expanded/collapsed state
    //////////////////////////////////////////////////////////////////////////////////
    setExpanded(range: string) {
      // a-z range, which panel is expanded in display
      this._userService.expandedPanelUserList = range;
    }
    collapseExpanded() {
      // all panels are collapsed - currently not used
      this._userService.expandedPanelUserList = '';
    }
    //////////////////////////////////////////////////////////////////////////////////
    // on pressing edit/delete button, the event is passed to parent component for processing
    //////////////////////////////////////////////////////////////////////////////////
    editUser(user: IUser): void {
      this.editUserEvent.emit(user);
    }
    removeUser(name: string): void {
      this.removeUserEvent.emit(name);
    }
    //////////////////////////////////////////////////////////////////////////////////
    // sort data to be displayed in ascending alphabetical order
    //////////////////////////////////////////////////////////////////////////////////
    sortData() {
      for (let i = 0; i < this.groupList.length; i++) {
        this.userShow[this.groupList[i]] = new Array();
      }
      this._users.forEach(element => {
        for (let i = this.range.length; i >= 0; i--) {
          if (element.userName.toLocaleLowerCase() >= this.range[i]) {
            this.userShow[this.groupList[i]].push(element);
            break;
          }
        }
      });
    }
    //////////////////////////////////////////////////////////////////////////////////
    // set bugs by filterBy owner name. the filtered bugs will be shown
    // under the owner name
    //////////////////////////////////////////////////////////////////////////////////
    updateFilter(): void {
      const filterBy: string = this._filterBy.toLowerCase();
      this._userService.expandedPanelUserList = '';
      this._userService.filterBy = filterBy;
      this.filtered = [];
      if(filterBy !== '') {
      this.collapseExpanded(); // only filtered panel will be expanded
      this._users.forEach(element => {
        if (element.userName.toLocaleLowerCase().indexOf(filterBy) === 0) {
          this.filtered.push(element);
        }
      });
    }
  }
}