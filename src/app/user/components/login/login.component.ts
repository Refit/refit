import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { messageList } from '../../messages';
import { IUser } from '../../interfaces/iuser';
import { slideInAnimation } from '../../../shared/animation/slide-in';
@Component({
  selector: 'user-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [slideInAnimation],
  host: {
    '[@slideInAnimation]': '{value: ": enter", params: { opacity: 0.9 }}'
  }
})
export class LoginComponent implements OnInit {
  formError: string = null;
  submitting = false;
  user: IUser = {
    userName: '',
    password: '',
  };
  rememberMe: boolean;
  returnUrl: string;
  adminUrl: string;
  hide: boolean = true;
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _authService: AuthService,
    private _messageService: MessageService) { }
  //////////////////////////////////////////////////////////////////////////////////
  // ngOnInit
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    // reset login status
    this._authService.logout();
    this.rememberMe = false;
    // get return url from route parameters or default to '/home'
    if (this._route.snapshot.queryParams['returnUrl']) {
      this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/home';
    } else {
      this.returnUrl = '/home';
    }
    this.adminUrl = '/admin';
  }
    // go to home page on pressing x icon
    goHome() {
      this._router.navigate(['/home']);
    }
  //////////////////////////////////////////////////////////////////////////////////
  // login form submit event
  //////////////////////////////////////////////////////////////////////////////////
  onSubmit(loginForm: NgForm) {
    // clear old messages if they are still active
    this._messageService.clear();
    this.formError = null;
    this.submitting = true;
    // send request to server
    this._authService.login(this.user, this.rememberMe)
      .subscribe(
        // data returned from the server
        data => {
          if (data && data.length) { // there is a valid response from HTTP
            if (data === 'A') { // if admin login
              this._router.navigate([this.adminUrl]);
            } else {            // else user login
              this._router.navigate([this.returnUrl]);
            }
          } else {
            this._messageService.add(messageList['userNotfound']);
            this.formError = messageList['userNotfound']['detail'];
          }
          this.submitting = false;
        },
        // exceptional termination of the observable sequence
        error => {
          this._messageService.add(messageList['invalidTermination']);
          this.formError = messageList['invalidTermination']['detail'];
          this.submitting = false;
        });
  }
}