import { Component, OnInit, Input, Inject, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IUser } from '../../interfaces/iuser';
import { UsersService } from '../../services/users.service';
@Component({
  selector: 'user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {
  private _user: IUser;
  selectedValue: string;
  userTypes = [
    {value: 'U', viewValue: 'User'},
    {value: 'A', viewValue: 'Admin'},
  ];
  @Input() set user(us: IUser) {
    this._user = us;
  }
  get user() {
    return this._user;
  }
  @Output() editUsEvent = new EventEmitter<IUser>();
  constructor(public dialog: MatDialog, private _userService: UsersService) { }
  ngOnInit() {
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(UserUpdateDialog, {
      width: '400px',
      data: { userName: this.user.userName, email: this.user.email, isAdmin: this.user.isAdmin,
        userTypes: this.userTypes, selected: this.selectedValue }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
      this.user.email = result.email;
      if (result.selected != null){
        this.user.isAdmin = result.selected;
      }
      this._userService.currentUser = this.user;
      this.editUsEvent.emit(this.user);
       }
    });
  }
}

@Component({
  selector: 'user-update-dialog',
  templateUrl: './user-update-dialog.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateDialog {
  encapsulation: ViewEncapsulation.None;
  constructor(
    public dialogRef: MatDialogRef<UserUpdateDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
  onNoClick(): void {
    this.dialogRef.close(this.data);
  }
}