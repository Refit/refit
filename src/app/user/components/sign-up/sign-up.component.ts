import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';
import { AuthService } from '../../services/auth.service';
import { messageList } from '../../messages';
import { IUser } from '../../interfaces/iuser';
import { NgForm } from '@angular/forms';
import { slideInAnimation } from '../../../shared/animation/slide-in';
@Component({
  selector: 'user-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  animations: [slideInAnimation],
  // attach the fade in animation to the host (root) element of this component
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[@slideInAnimation]': '{value: ": enter", params: { opacity: 0.9 }}'
  }
})
export class SignUpComponent implements OnInit {
  user: IUser = {
    userName: '',
    password: '',
    email: '',
  };
  passwordConfirmError: boolean = false;
  passwordConfirm: string = '';
  formError: string = null;
  submitting = false;
  constructor(private authService: AuthService, private _router: Router,
    private _messageService: MessageService) { }
  ngOnInit() { }
  // go to home page on pressing x icon
  goHome() {
    this._router.navigate(['/home']);
  }
  //////////////////////////////////////////////////////////////////////////////////
  // signup form submit event
  //////////////////////////////////////////////////////////////////////////////////
  public onFormSubmit(signUpForm: NgForm) {
    // clear old messages if they are still active
    this._messageService.clear();
    this.formError = null;
    this.submitting = true;
    this.authService.signup(this.user).subscribe(
      data => {
        if (data !== 'Exists') {
          this._router.navigateByUrl('/welcome/log-in');
        } else {
          this._messageService.add(messageList['userExists']);
          this.formError = messageList['userExists']['detail'];
        }
        this.submitting = false;
      },
      // exceptional termination of the observable sequence
      error => {
        this._messageService.add(messageList['invalidTermination']);
        this.formError = messageList['invalidTermination']['detail'];
        this.submitting = false;
      }
    );
  }
  //////////////////////////////////////////////////////////////////////////////////
  // check form errors that are not checked in the template
  //////////////////////////////////////////////////////////////////////////////////
  checkPasswords(): void {
    this.passwordConfirmError = this.user.password !== this.passwordConfirm;
  }
}