import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// our components
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
export const routes: Routes = [
  {
    path: 'log-in',
    component: LoginComponent,
  },
  {
      path: 'sign-up',
      component: SignUpComponent,
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
export const routableComponents = [
  LoginComponent,
  SignUpComponent,
];