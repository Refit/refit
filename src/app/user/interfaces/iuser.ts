/* Defines the user entity */
export interface IUser {
    email?: string;
    userName: string;
    password?: string;
    isAdmin?: string;
    id?: number;
}