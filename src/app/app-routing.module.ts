import { Routes, RouterModule } from '@angular/router';
// modules
import { AdminModule } from './admin/admin.module';
import { HomeModule } from './home/home.module';
// components
import { WelcomeComponent } from './components/welcome/welcome.component';
import { LoginComponent } from './user/components/login/login.component';
import { SignUpComponent } from './user/components/sign-up/sign-up.component';
// core services
import { NgModule } from '@angular/core';
import { PreloadStrategyService } from './core/preload-strategy.service';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AuthGuardService } from './user/services/auth-guard.service';
import { CoverComponent } from './components/cover/cover.component';
import { ContactUsComponent } from './global/components/contact-us/contact-us.component';
import { AboutUsComponent } from './global/components/about-us/about-us.component';
import { TermsComponent } from './global/components/terms/terms.component';
// root routing - top level
const routes: Routes = [
  {
    // default path
    path: '', redirectTo: 'welcome', pathMatch: 'full'
  },
  {
    path: 'welcome',
    component: WelcomeComponent,
    children: [
      {
        path: '', redirectTo: 'guest', pathMatch: 'full'
      },
      {
        path: 'guest',
        component: CoverComponent,
      },
      {
        path: 'log-in',
        component: LoginComponent,
      },
      {
        path: 'sign-up',
        component: SignUpComponent,
      },
      {
        path: 'about',
        component: AboutUsComponent,
      },
      {
        path: 'contact-us',
        component: ContactUsComponent,
      },
      {
        path: 'terms',
        component: TermsComponent,
      },
    ]
  },
  {
    path: 'home',
    canActivate: [AuthGuardService],
    // component: HomeComponent,
    // loadChildren: './home/home.module#HomeModule'
    // lazy load with preload is the best configuration
    loadChildren: () => HomeModule, // lazy load
    data: { preload: true }  // preload in background to be ready to use when user navigates to home
  },
  {
    path: 'admin',
    canLoad: [AuthGuardService],
    // component: AdminComponent,
    // lazy load with preload is the best configuration
    loadChildren: () => AdminModule, // lazy load
    data: { preload: true }  // preload in background to be ready to use when user navigates to home
  },
  {
    // not legal path
    path: '**',
    component: PageNotFoundComponent
  }
];
@NgModule({
  imports: [
    // preload for all lazy modules
    // RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    // custom module preload if preload: true is defined
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadStrategyService })
    // Eager loading - immediate on application start
    // RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: [PreloadStrategyService]
})
export class AppRoutingModule { }
export const routedComponents = [
  WelcomeComponent,
  PageNotFoundComponent,
  CoverComponent
];