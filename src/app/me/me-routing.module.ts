import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TraineeMainComponent } from './components/trainee-main/trainee-main.component';
import { HelloTraineeComponent } from './components/hello-trainee/hello-trainee.component';
import { ProgramModule } from '../program/program.module';
export const routes: Routes = [
  {
    path: '',
    component: TraineeMainComponent,
    children: [
      {
        path: '', redirectTo: 'hello', pathMatch: 'full'
      },
      {
        path: 'hello',
        component: HelloTraineeComponent,
      },
      {
        path: 'program',
        loadChildren: () => ProgramModule, // lazy load
        data: { preload: true }  // preload in background to be ready to use when user navigates to home
      },
    ]
  },
];
@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class MeRoutingModule { }
export const routedComponents = [
  TraineeMainComponent,
  HelloTraineeComponent,
];