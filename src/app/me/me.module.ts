import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { MeRoutingModule, routedComponents } from './me-routing.module';
import { TraineeMainComponent } from './components/trainee-main/trainee-main.component';
import { HelloTraineeComponent } from './components/hello-trainee/hello-trainee.component';
import { MenuService } from './services/menu.service';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    MeRoutingModule,
  ],
  declarations: [
    routedComponents,
  ],
  providers: [
    MenuService
  ],
  exports: [
    TraineeMainComponent,
    HelloTraineeComponent,
  ]
})
export class MeModule { }