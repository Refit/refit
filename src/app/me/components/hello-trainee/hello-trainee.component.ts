import { Component, OnInit } from '@angular/core';
import { TraineeService } from '../../../trainee/services/trainee.service';
import { MenuService } from '../../services/menu.service';
import { iTraineeDetails } from '../../../trainee/interfaces/iTraineeDetails';
@Component({
  selector: 'app-hello-trainee',
  templateUrl: './hello-trainee.component.html',
  styleUrls: ['./hello-trainee.component.scss']
})
export class HelloTraineeComponent implements OnInit {
  traineeName: string;
  trainee: iTraineeDetails;
  newUser: boolean = true;
  constructor(private _traineeService: TraineeService, private _menuService: MenuService) {
  }
  ngOnInit() {
    this.traineeName = this._traineeService.currentTrainee.traineeDetails.userName;
    this.trainee = JSON.parse(JSON.stringify(this._traineeService.currentTrainee.traineeDetails));
    if( this._traineeService.currentTrainee.traineeDetails.strength != 0 )
      {
        this.newUser = false;
      }
  }
}