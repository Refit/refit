import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelloTraineeComponent } from './hello-trainee.component';

describe('HelloTraineeComponent', () => {
  let component: HelloTraineeComponent;
  let fixture: ComponentFixture<HelloTraineeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelloTraineeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelloTraineeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
