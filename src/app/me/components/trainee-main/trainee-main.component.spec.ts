import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraineeMainComponent } from './trainee-main.component';

describe('TraineeMainComponent', () => {
  let component: TraineeMainComponent;
  let fixture: ComponentFixture<TraineeMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraineeMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraineeMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
