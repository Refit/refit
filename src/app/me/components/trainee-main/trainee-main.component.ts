import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { INavLinks } from '../../../shared/interfaces/inav-links';
import { navLinksList } from '../../navigation-links';
import { AuthService } from '../../../user/services/auth.service';
import { MessageService } from '../../../../../node_modules/primeng/components/common/messageservice';
import { TraineeService } from '../../../trainee/services/trainee.service';
import { Itrainee } from '../../../trainee/interfaces/itrainee';
import { messageList } from '../../../messages';
import { MenuService } from '../../services/menu.service';
@Component({
  selector: 'trainee-main',
  templateUrl: './trainee-main.component.html',
  styleUrls: ['./trainee-main.component.scss']
})
export class TraineeMainComponent implements OnInit {
  navLinks: INavLinks[] = navLinksList;
  constructor(private _traineeService: TraineeService ,
    private _route: ActivatedRoute,
    private _messageService: MessageService, private _authService: AuthService , private _menuService: MenuService) {
    // the following is for the case where the data can be changed while staying in the same route.
    // since trainee can't be changed, we use snapshot in OnInit instead of the following code   
    }
  ngOnInit() {
    this._traineeService.currentTrainee = this._route.snapshot.data['trainee'];
    this._menuService.setMenu(this._traineeService.currentTrainee.traineeDetails);
  }
  getTrainee(): void {
    // clear old messages if they are still active
    this._messageService.clear();
    this._traineeService.getTrainee(this._authService.currentUser.userName).subscribe(
      (data: Itrainee) => {
        if (data) {
          this._traineeService.currentTrainee = data;
        }
      },
      // exceptional termination of the observable sequence
      error => {
        // global message defined in app messages.ts
        this._messageService.add(messageList['invalidTermination']);
      }
    );
  }
}