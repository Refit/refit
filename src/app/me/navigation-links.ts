import { INavLinks } from '../shared/interfaces/inav-links';
export const navLinksList: INavLinks[] = [
    {
        label: 'Personal Details',
        route: ['program', 'details'],
        icon: '',
    },
    {
        label: 'Start Session',
        route: ['program' , 'session'],
        icon: '',
    },
    {
        label: 'Progress',
        route: ['program', 'progress'],
        icon: '',
    },
];