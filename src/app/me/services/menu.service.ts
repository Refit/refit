import { Injectable, OnInit } from '@angular/core';
import { iTraineeDetails } from '../../trainee/interfaces/iTraineeDetails';
import { TraineeService } from '../../trainee/services/trainee.service';
@Injectable()
export class MenuService implements OnInit {
  activateMenu: any = {
    details: true,
    health: false,
    goal: false,
    summary: false,
    build: false,
    session: false,
    progress: false,
  };
  trainee: iTraineeDetails;
  constructor(private _traineeService: TraineeService) { }
  ngOnInit() {
  }
  setMenu(trainee: iTraineeDetails): void {
    // check if can acivate health
    if (trainee.fname !== null) {
      this.activateMenu.health = true;
    }
    // check if can activate goal
    if (trainee.fname !== null) {
      this.activateMenu.goal = true;
    }
    // check if can activate summary and build
    if (trainee.target !== null) {
      this.activateMenu.summary = true;
      this.activateMenu.build = true;
      this.activateMenu.progress = true;
    }
    if (+trainee.strength !== 0) {
      this.activateMenu.session = true;
    }
  }
}